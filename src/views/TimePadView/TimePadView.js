/* @flow */
import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { increment, saveInputBox, chooseOutputFormat, collapse_input,
		     update_node_title_and_hide_editable,
		     update_item_title_and_hide_editable, update_node_title, set_node_title_editable,
		     saveInputPeriodically, set_item_title_editable, sort_inputs} from '../../redux/modules/timePad'
import { save_snapshot, past_snapshot_by_step, clean_time_pad_in_present,
		     future_snapshot_by_step, choose_next_future, set_demo_mode,
	       insert_between_current_and_children, delete_current_snapshot,
				 delete_current_and_children, pull_change_from_server, push_change_to_server,
		     choose_current_snapshot, present_past_switch, replace_current,
		     change_past_from_present, get_snapshot_with_hash, delete_file_for_doc,
		     upload_file_for_doc, delete_input_from_timeline,
		     delete_input_local_from_timeline } from '../../redux/modules/timeLiner'
import App from '../../components/TimePad'
import { ActionCreators } from 'redux-undo';
import {random_hash, get_timeline_nodes_with_current} from '../../concerns/helpers'

export class TimePadView extends React.Component<void, Props, void> {
	constructor(props) {
		super(props)
		this.props.increment()
	}



	static propTypes = {
		inputs: PropTypes.array.isRequired,
		history_frames: PropTypes.array.isRequired,
		current_snapshot: PropTypes.number.isRequired,
		my_state: PropTypes.object.isRequired,
		file_spinner: PropTypes.string.isRequired,
		demo_mode: PropTypes.string.isRequired,

		//timeLiner: PropTypes.object.isRequired,
		increment: PropTypes.func.isRequired,
		boxChangeEvent: PropTypes.func.isRequired,
		chooseOutputFormat: PropTypes.func.isRequired,
		saveInputPeriodically: PropTypes.func.isRequired,
		undo: PropTypes.func.isRequired,
		redo: PropTypes.func.isRequired,
		save_snapshot: PropTypes.func.isRequired,
		past_snapshot_by_step: PropTypes.func.isRequired,
		future_snapshot_by_step: PropTypes.func.isRequired,
		choose_next_future: PropTypes.func.isRequired,
		change_past_from_present: PropTypes.func.isRequired,
		choose_current_snapshot: PropTypes.func.isRequired,
		time_status: PropTypes.object.isRequired,
		present_past_switch: PropTypes.func.isRequired,
		replace_current: PropTypes.func.isRequired,
		insert_between_current_and_children: PropTypes.func.isRequired,
		delete_current_snapshot: PropTypes.func.isRequired,
		delete_current_and_children: PropTypes.func.isRequired,
		pull_change_from_server: PropTypes.func.isRequired,
		push_change_to_server: PropTypes.func.isRequired,
		upload_file_for_doc: PropTypes.func.isRequired,
		delete_file_for_doc: PropTypes.func.isRequired,
		delete_input: PropTypes.func.isRequired,
		delete_input_local: PropTypes.func.isRequired,
		collapse_input: PropTypes.func.isRequired,
		sort_inputs: PropTypes.func.isRequired,
		set_item_title_editable: PropTypes.func.isRequired,
		update_item_title_and_hide_editable: PropTypes.func.isRequired,
		update_node_title: PropTypes.func.isRequired,
		set_node_title_editable: PropTypes.func.isRequired,
		update_node_title_and_hide_editable: PropTypes.func.isRequired,
		node_title: PropTypes.string.isRequired,
		node_title_editable: PropTypes.string.isRequired,
		clean_time_pad_in_present: PropTypes.func.isRequired,
		set_demo_mode: PropTypes.func.isRequired
	};




	render () {
				main_module.react_trigger = 'yes'
				main_module.viewed_hash = this.props.time_status.viewed_snapshot
		    main_module.current_snapshot = this.props.current_snapshot
				main_module.time_lines = get_timeline_nodes_with_current(this.props.history_frames, this.props.current_snapshot)


		return <App inputs={this.props.inputs}
								demo_mode={this.props.demo_mode}
								file_spinner={this.props.file_spinner}
								my_state={this.props.my_state}
								current_snapshot={this.props.current_snapshot}
								timeLiner={this.props.timeLiner}
								increment={this.props.increment}
								chooseOutputFormat={this.props.chooseOutputFormat}
								boxChangeEvent={this.props.boxChangeEvent}
								saveInputPeriodically={this.props.saveInputPeriodically}
								undo={this.props.undo}
								redo={this.props.redo}
								save_snapshot={this.props.save_snapshot}
								past_snapshot_by_step={this.props.past_snapshot_by_step}
								future_snapshot_by_step={this.props.future_snapshot_by_step}
								choose_next_future={this.props.choose_next_future}
								change_past_from_present={this.props.change_past_from_present}
								//get_snapshot_with_hash={this.props.get_snapshot_with_hash}
								get_snapshot_with_hash={get_snapshot_with_hash}
								choose_current_snapshot={this.props.choose_current_snapshot}
								update_inputs={this.props.update_inputs}
								time_status={this.props.time_status}
								present_past_switch={this.props.present_past_switch}
								replace_current={this.props.replace_current}
								insert_between_current_and_children={this.props.insert_between_current_and_children}
								delete_current_snapshot={this.props.delete_current_snapshot}
								delete_current_and_children={this.props.delete_current_and_children}
								pull_change_from_server={this.props.pull_change_from_server}
								push_change_to_server={this.props.push_change_to_server}
								upload_file_for_doc={this.props.upload_file_for_doc}
								delete_file_for_doc={this.props.delete_file_for_doc}
								delete_input={this.props.delete_input}
								delete_input_local={this.props.delete_input_local}
								collapse_input={this.props.collapse_input}
								sort_inputs={this.props.sort_inputs}
								set_item_title_editable={this.props.set_item_title_editable}
								update_item_title_and_hide_editable={this.props.update_item_title_and_hide_editable}
								update_node_title={this.props.update_node_title}
								set_node_title_editable={this.props.set_node_title_editable}
								update_node_title_and_hide_editable={this.props.update_node_title_and_hide_editable}

								node_title={this.props.node_title}
								node_title_editable={this.props.node_title_editable}
								clean_time_pad_in_present={this.props.clean_time_pad_in_present}
								set_demo_mode={this.props.set_demo_mode}
						/>
	}
}


const mapStateToProps = (state) => ({
	my_state: state,
	inputs: state.timePad.present.inputs,
	history_frames: state.timePad.history_frames,
	current_snapshot: state.timePad.current_snapshot,
	time_status: state.timePad.time_status,
	file_spinner: state.timePad.file_spinner,
	node_title: state.timePad.present.node_title,
	node_title_editable: state.timePad.present.node_title_editable,
	demo_mode: state.timePad.demo_mode
})

export default connect((mapStateToProps), {
	increment: () => increment(random_hash()),
	boxChangeEvent: (index, input_text) => saveInputBox(index, input_text),
	chooseOutputFormat: (index, output_format) => chooseOutputFormat(index, output_format),
	saveInputPeriodically: (index, input_text) => saveInputPeriodically(index, input_text),
	undo: () => ActionCreators.undo(),
	redo: () => ActionCreators.redo(),
	save_snapshot: (hash, description) => save_snapshot(hash, description),
	past_snapshot_by_step: () => past_snapshot_by_step(1),
	future_snapshot_by_step: () => future_snapshot_by_step(1),
	choose_next_future: ( my_hash ) => choose_next_future( my_hash ),
	change_past_from_present: ( my_past_hash ) => change_past_from_present( my_past_hash ),
	choose_current_snapshot: (chosen_hash) => choose_current_snapshot(chosen_hash),
	present_past_switch: ( tense ) => present_past_switch( tense ),
	replace_current: () => replace_current(),
	insert_between_current_and_children: () => insert_between_current_and_children(),
	delete_current_snapshot: () => delete_current_snapshot(),
	delete_current_and_children: () => delete_current_and_children(),
	pull_change_from_server: (slug_id) => pull_change_from_server(slug_id),
	push_change_to_server: ( state_doc ) => push_change_to_server(state_doc),
	upload_file_for_doc: ( selector, hash_id ) => upload_file_for_doc(selector, hash_id),
	delete_file_for_doc: (input_uid, uid) => delete_file_for_doc(input_uid, uid),
	delete_input: (input_uid) => delete_input_from_timeline( input_uid ),
	delete_input_local: (input_uid) => delete_input_local_from_timeline( input_uid ),
	collapse_input: (is_collapse, input_id) => collapse_input(is_collapse, input_id),
	sort_inputs: (input_id, new_position) => sort_inputs(input_id, new_position),
	set_item_title_editable: (input_id, is_editable) => set_item_title_editable (input_id, is_editable),
	update_item_title_and_hide_editable: ( input_id, title_content ) => update_item_title_and_hide_editable( input_id, title_content ),
	update_node_title: (title_content) => update_node_title ( title_content ),
	set_node_title_editable: (is_editable) => set_node_title_editable (is_editable),
	update_node_title_and_hide_editable: (title_content) =>update_node_title_and_hide_editable( title_content ),
	clean_time_pad_in_present: () => clean_time_pad_in_present(),
	set_demo_mode: (my_mode) => set_demo_mode(my_mode)
})(TimePadView)
