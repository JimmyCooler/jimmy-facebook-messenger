import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import counter from './modules/counter'
import timePad from './modules/timePad'
import timeLineEnhancer from './modules/timeLiner'
import { SAVE_INPUT_BOX } from './modules/timePad'
import undoable, { distinctState, includeAction, excludeAction } from 'redux-undo';



export default combineReducers({
  counter,
  //timeLiner,
  timePad: timeLineEnhancer(
    undoable(timePad, {
      limit: 200,
      filter: excludeAction(SAVE_INPUT_BOX) })
  ),
  router
})


/*
export default combineReducers({
  counter,
  //timeLiner,
  timePad: timeLineEnhancer(
    undoable(timePad, {
      limit: 200,
      filter: excludeAction(SAVE_INPUT_BOX) })
  ),
  router
})
*/
