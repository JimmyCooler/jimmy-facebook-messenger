// ------------------------------------
// Constants
// ------------------------------------
export const SAVE_SNAPSHOT = 'SAVE_SNAPSHOT'
export const UPDATE_SNAPSHOT = 'UPDATE_SNAPSHOT'
export const PAST_SNAPSHOT_BY_STEP = 'PAST_SNAPSHOT_BY_STEP'
export const FUTURE_SNAPSHOT_BY_STEP = 'FUTURE_SNAPSHOT_BY_STEP'
export const CHOOSE_NEXT_FUTURE ='CHOOSE_NEXT_FUTURE'
export const CHANGE_PAST_FROM_PRESENT = 'CHANGE_PAST_FROM_PRESENT'
export const CHOOSE_CURRENT_SNAPSHOT = 'CHOOSE_CURRENT_SNAPSHOT'
export const PRESENT_PAST_SWITCH = 'PRESENT_PAST_SWITCH'
export const SET_AS_PRESENT = 'SET_AS_PRESENT'
export const REPLACE_CURRENT = 'REPLACE_CURRENT'
export const INSERT_BETWEEN_CURRENT_AND_CHILDREN = 'INSERT_BETWEEN_CURRENT_AND_CHILDREN'
export const DELETE_CURRENT_SNAPSHOT = 'DELETE_CURRENT_SNAPSHOT'
export const DELETE_CURRENT_AND_CHILDREN = 'DELETE_CURRENT_AND_CHILDREN'
export const SERVER_FETCH = 'SERVER_FETCH'
export const UPDATE_INPUTS_FROM_TIMELINE = 'UPDATE_INPUTS_FROM_TIMELINE'
export const DELETE_INPUTS_LOCAL_FROM_TIMELINE = 'DELETE_INPUTS_LOCAL_FROM_TIMELINE'
export const SET_FILE_LOADING = 'SET_FILE_LOADING'
export const CLEAN_TIME_PAD_IN_PRESENT = 'CLEAN_TIME_PAD_IN_PRESENT'
export const SET_DEMO_MODE = 'SET_DEMO_MODE'



//update_inputs_from_timeline

// ------------------------------------
// Actions
// ------------------------------------
export function set_loading (load_status) {
	return {
		type: SET_FILE_LOADING,
		payload: {
			load_status: load_status
		}
	}
}



export function save_snapshot (description) {
	return {
		type: SAVE_SNAPSHOT,
		payload: {
			description: description
		}
	}
}

export function update_snapshot (snapshot_id) {
	return {
		type: UPDATE_SNAPSHOT,
		payload: {
			snapshot_id: snapshot_id
		}
	}
}

export function past_snapshot_by_step ( step ) {
	return {
		type: PAST_SNAPSHOT_BY_STEP,
		payload: {
			step_size: step
		}
	}
}

export function future_snapshot_by_step ( step ) {
	return {
		type: FUTURE_SNAPSHOT_BY_STEP,
		payload: {
			step_size: step
		}
	}
}

export function choose_next_future ( my_hash ) {
	return {
		type: CHOOSE_NEXT_FUTURE,
		payload: {
			chosen_future_hash: my_hash
		}
	}
}

export function change_past_from_present ( my_past_hash ) {
	return {
		type: CHANGE_PAST_FROM_PRESENT,
		payload: {
			my_chosen_past: my_past_hash
		}
	}
}

export function choose_current_snapshot ( my_hash ) {
	return {
		type: CHOOSE_CURRENT_SNAPSHOT,
		payload: {
			chosen_hash: my_hash
		}
	}
}

export function present_past_switch ( present_or_past ) {
	return {
		type: PRESENT_PAST_SWITCH,
		payload: {
			tense: present_or_past
		}
	}
}

export function set_as_present (  ) {
	return {
		type: SET_AS_PRESENT
	}
}

export function replace_current ( ) {
	return {
		type: REPLACE_CURRENT,
		payload: {
			description: ''
		}
	}
}

export function insert_between_current_and_children ( ) {
	return {
		type: INSERT_BETWEEN_CURRENT_AND_CHILDREN,
		payload: {
			description: ''
		}
	}
}

export function delete_current_snapshot ( ) {
	return {
		type: DELETE_CURRENT_SNAPSHOT
	}
}

export function delete_current_and_children ( ) {
	return {
		type: DELETE_CURRENT_AND_CHILDREN
	}
}



export function server_fetch ( fetched_state ) {
	return {
		type: SERVER_FETCH,
		payload:{
			my_state: fetched_state
		}
	}
}


export function update_inputs_from_timeline (inputs) {
	return {
		type: UPDATE_INPUTS_FROM_TIMELINE,
		payload:{
			inputs: inputs
		}
	}
}

export function pull_change_from_server (slug_id) {
	return dispatch => {
		if( ! slug_id ){
			return Promise.resolve()
		}

		const url = 'http://localhost:4000/docs/' + slug_id + '.json'
		dispatch( set_loading('active') )
		return fetch(url)
					.then(function(response) {
						if (response.status >= 400) {
							throw new Error("Bad response from server");
						}
						return response.json();
					})
					.then(function(all) {
				    let content = LZString.decompressFromEncodedURIComponent(all.content)
						content = JSON.parse(content)
						//const content = JSON.parse(all.content)
						const tmp_state = content.timePad
						dispatch( server_fetch(tmp_state) )
						dispatch( set_loading('none') )
					})
				.catch(function(all){
					dispatch( set_loading('none') ) //in case the process failed (for the first time)
				})

	}
}

export function push_change_to_server( state_doc ) {
	return (dispatch, getState) => {
		if( ! state_doc.url_slug ){
			return Promise.resolve()
		}

		const url = 'http://localhost:4000/docs/' + state_doc.url_slug + '.json'

		const compressed_content = LZString.compressToEncodedURIComponent(
			JSON.stringify( getState() )
		);
		const new_state_doc = Object.assign({}, state_doc,
			{ content: compressed_content })

		/*const new_state_doc = Object.assign({}, state_doc,
				{ content: JSON.stringify( getState() ) })*/
		const data = {
			state_doc: new_state_doc
		}
		const str = JSON.stringify(data)

		return fetch(url, {
				//credentials: 'include', //pass cookies, for authentication
				method: 'post',
				headers: {
					'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
					//'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					'Content-Type': 'application/json'
				},
				body: str
			})
			.then(function (response) {
				if (response.status >= 400) {
					throw new Error("Bad response from server")
				}
				return response.json()
			}, function (error) {
			})
			.then(function (stories) {
				//console.log('the stories are ==>', stories);
			});
	}
}

export function upload_file_for_doc ( selector, new_id) {
	return (dispatch, getState) => {
		const base_url = 'http://localhost:4000'
		const input = document.querySelector(selector);
		const new_hash_id = new_id
		var data = new FormData()
		data.append('uploaded_file[state_doc_hash]', main_module.slug_id)
		data.append('uploaded_file[content]', input.files[0])

		dispatch( set_loading('active') )
		fetch('http://localhost:4000/uploaded_files.json', {
			method: 'POST',
			body: data
		})
				.then(function (response) {
					if (response.status >= 400) {
						throw new Error("Bad response from server")
					}else{
						return response.json()
					}
				})
				.then(function (json) {
					const file_url = base_url + json.url
					const current_state = getState()
					const current_inputs = current_state.timePad.present.inputs
					const new_inputs = [
						...current_inputs,
						{
							id: new_id,
							format: 'file',
							file_type: json.ext,
							file_url: file_url,
							file_name: json.file_name,
							file_uid: json.uid,
							is_collapse: 'no'
						}
					]

					dispatch(update_inputs_from_timeline( new_inputs ))
					dispatch( set_loading('none') )
					const state_doc = {
						is_public: false,
						url_slug:  main_module.slug_id
					}
					dispatch( push_change_to_server( state_doc ) )
				})
	}
}

export function delete_file_for_doc (input_uid, uid) {
	return (dispatch, getState) => {
		const base_url = 'http://localhost:4000'
		var data = new FormData()
		data.append('_method', 'DELETE')
		data.append('uid', uid)
		fetch('http://localhost:4000/content/', {
			method: 'POST',
			body: data
		})
				.then(function (response) {
					if (response.status >= 400) {
						throw new Error("Bad response from server")
					}else{
						return response.json()
					}
				})
				.then(function (json) {
					if(json.status && json.status == 'success'){
						dispatch( delete_input_from_timeline (input_uid) )
					}
				})

	}
}

export function delete_input_from_timeline (input_uid) {
	return (dispatch, getState) => {
		dispatch( delete_input_local_from_timeline(input_uid) )
		let current_state = getState()
		const time_status = current_state.timePad.time_status
		const current_present = current_state.timePad.present
		if(time_status.mode == "time_travel"){
			dispatch( update_snapshot (time_status.viewed_snapshot) )
		}


		const state_doc = {
			is_public: false,
			url_slug:  main_module.slug_id
		}
		dispatch( push_change_to_server( state_doc ) )
	}
}

export function delete_input_local_from_timeline (input_uid) {
	return {
		type: DELETE_INPUTS_LOCAL_FROM_TIMELINE,
		payload:{
			uid: input_uid
		}
	}
}

export function clean_time_pad_in_present () {
	return {
		type: CLEAN_TIME_PAD_IN_PRESENT
	}
}


export function set_demo_mode (my_mode) {
	return {
		type: SET_DEMO_MODE,
		payload:{
			mode: my_mode
		}
	}
}


/*export function fetchDoc() {
	return dispatch => {
		dispatch(requestPosts(subreddit))
		return fetch(`http://www.reddit.com/r/${subreddit}.json`)
			.then(response => response.json())
			.then(json => dispatch(receivePosts(subreddit, json)))
	}
}*/



export const actions = {
	save_snapshot,
	update_snapshot,
	past_snapshot_by_step,
	future_snapshot_by_step,
	choose_next_future,
	change_past_from_present,
	choose_current_snapshot,
	present_past_switch,
	replace_current,
	insert_between_current_and_children,
	delete_current_snapshot,
	delete_current_and_children,
	server_fetch,
	pull_change_from_server,
	push_change_to_server,
	upload_file_for_doc,
	delete_input_from_timeline,
	delete_input_local_from_timeline,
	clean_time_pad_in_present,
	set_demo_mode
	//upload_file_for_doc
}



// ------------------------------------
// Action Handlers
// ------------------------------------


const ACTION_HANDLERS = {

	[SET_FILE_LOADING]: (state, action) => {
		if( action.payload.load_status ) {
			return Object.assign({}, state, {file_spinner: action.payload.load_status})
		}
		return state
	},

	[SAVE_SNAPSHOT]: (state, action) => {
		if (!(state.present && action.payload.description)) {
			return state
		}

		const payload = action.payload
		const hash_id = state.counter + 1
		const new_snapshots = [
			...state.snapshots,
			{
				id: hash_id,
				state: state.present
			}
		]

		let new_histories = []
		let new_snapshot_siblings = [hash_id]

		if (state.history_frames) {
			new_histories = state.history_frames.map(history_frame => {
						if (history_frame.id === state.current_snapshot) {
							const children = history_frame.children
							const new_children = [...children, hash_id]
							new_snapshot_siblings = new_children
							return {
								id: history_frame.id,
								description: history_frame.description,
								children: new_children,
								parent: history_frame.parent
							}
						}
						else {
							return history_frame
						}
					}
			)
		}

		new_histories = [
			...new_histories,
			{
				id: hash_id,
				description: payload.description,
				children: [],
				parent: state.current_snapshot
			}
		]


		return Object.assign({}, state, {
										snapshots: new_snapshots,
										history_frames: new_histories,
										current_snapshot: hash_id,
										counter: hash_id,
										time_status: {
											mode: 'present',
											viewed_snapshot: hash_id,
											parent: state.current_snapshot,
											children: [],
											snapshot_siblings: new_snapshot_siblings,
											present_freeze: ''  //no need to freeze present since it is already in "present" mode
										}
								})

	},

	[UPDATE_SNAPSHOT]: (state, action) => {
		if (action.payload.snapshot_id && state.snapshots) {

			const new_snapshots = state.snapshots.map(new_snapshot => {
						if (new_snapshot.id === action.payload.snapshot_id) {
							return {
								id: action.payload.snapshot_id,
								state: state.present
							}
						}
						else {
							return new_snapshot
						}
					}
			)

			return Object.assign({}, state, {snapshots: new_snapshots})
		}
		return state
	},


	[PAST_SNAPSHOT_BY_STEP]: (state, action) => {
		//return state
		if (state.time_status.parent && state.history_frames && state.past
				&& state.present && state.snapshots) {

			if (state.current_snapshot === state.time_status.viewed_snapshot) {
				const latest_snapshot = get_snapshot_with_hash(state.snapshots, state.current_snapshot)
				const latest_state = latest_snapshot.state
				if (latest_state != state.present) {
					const latest_past = [...state.past, state.present]
					const latest_time_status = Object.assign({}, state.time_status,
							{
								mode: 'time_travel',
								present_freeze: state.present

							})
					return Object.assign({}, state, {
						time_status: latest_time_status,
						past: latest_past,
						present: latest_state
					})
				}
			}

			let my_time_status = state.time_status
			const payload = action.payload
			const parent_frame = request_prev_history_frame(state, payload.step_size);

			const present_freeze = only_freeze_time_from_present_to_time_travel(state);
			let my_past = state.past
			let my_present = state.present


			if (parent_frame) {
				my_time_status = {
					mode: 'time_travel',
					viewed_snapshot: parent_frame.id,
					parent: parent_frame.parent,
					children: parent_frame.children,
					snapshot_siblings: [],
					present_freeze: present_freeze
				}


				//change past and present
				my_past = [...state.past, state.present]
				const my_snapshot = get_snapshot_with_hash(state.snapshots, parent_frame.id)
				my_present = my_snapshot.state
			}

			return Object.assign({}, state, {
									time_status: my_time_status,
									past: my_past,
									present: my_present
								})


		}
		return state
	},

	[FUTURE_SNAPSHOT_BY_STEP]: (state, action) => {
		//return state

		if (state.time_status.children && state.history_frames && state.present && state.past) {
			let my_time_status = state.time_status
			const payload = action.payload
			const parent_frame = request_next_history_frame(state, payload.step_size);
			let my_present = state.present

			let my_past = state.past


			if (parent_frame && parent_frame != 'no-child' && parent_frame != 'multiple') {
				my_time_status = {
					mode: 'time_travel',
					viewed_snapshot: parent_frame.id,
					parent: parent_frame.parent,
					children: parent_frame.children,
					snapshot_siblings: [parent_frame.id],
					present_freeze: only_freeze_time_from_present_to_time_travel(state)
				}

				//change past and present
				my_past = [...state.past, state.present]
				const my_snapshot = get_snapshot_with_hash(state.snapshots, parent_frame.id)
				my_present = my_snapshot.state
			}

			return Object.assign({}, state, {
				time_status: my_time_status,
				past: my_past,
				present: my_present
			})

		}
		return state
	},

	[CHOOSE_NEXT_FUTURE]: (state, action) => {
		if (state.time_status.children && state.time_status.children.length >= 1
				&& state.snapshots
				&& state.history_frames && action.payload.chosen_future_hash) {
			let my_time_status = state.time_status
			const payload = action.payload
			const siblings = my_time_status.children
			const hash_index = siblings.indexOf(payload.chosen_future_hash)

			if (hash_index >= 0) {
				let my_children = []
				for (var history_frame of state.history_frames) {
					if (history_frame.id == payload.chosen_future_hash) {
						my_children = history_frame.children
						break;
					}
				}

				my_time_status = {
					mode: 'time_travel',
					viewed_snapshot: payload.chosen_future_hash,
					parent: state.time_status.viewed_snapshot,
					children: my_children,
					snapshot_siblings: siblings,
					present_freeze: only_freeze_time_from_present_to_time_travel(state)
				}
			}

			const my_snapshot = get_snapshot_with_hash(state.snapshots, action.payload.chosen_future_hash)
			return Object.assign({}, state, {
				ime_status: my_time_status,
				past: [...state.past, state.present],
				present: my_snapshot.state
			})
			
		}
		return state
	},

	[CHANGE_PAST_FROM_PRESENT]: (state, action) => {
		//return state
		if (action.payload.my_chosen_past && state.current_snapshot) {
			return Object.assign({}, state, {current_snapshot: action.payload.my_chosen_past})
		}
		return state
	},

	[CHOOSE_CURRENT_SNAPSHOT]: (state, action) => {
		if (state.snapshots && state.history_frames
				&& state.history_frames.length >= 1
				&& action.payload.chosen_hash) {

			//let my_time_status = state.time_status
			const payload = action.payload
			const my_snapshot = get_snapshot_with_hash(state.snapshots, action.payload.chosen_hash)
			const my_history_frame = get_history_frame_with_hash(state.history_frames, action.payload.chosen_hash)
			if (!my_history_frame || !my_snapshot) {
				return state
			}

			let my_time_status = {
				mode: 'time_travel',
				viewed_snapshot: payload.chosen_hash,
				parent: my_history_frame.parent,
				children: my_history_frame.children,
				snapshot_siblings: [],
				present_freeze: only_freeze_time_from_present_to_time_travel(state)
			}

			return Object.assign({}, state, {
				time_status: my_time_status,
				past: [...state.past, state.present],
				present: my_snapshot.state
			})
		
		}

		return state

	},

	[PRESENT_PAST_SWITCH]: (state, action) => {
		if (action.payload.tense && state.time_status && state.time_status.mode && state.time_status.present_freeze) {
			let my_time_status = Object.assign({}, state.time_status, {mode: action.payload.tense})
			let my_present = state.present
			if (action.payload.tense == 'present') {
				my_present = state.time_status.present_freeze
			} else if (action.payload.tense == 'time_travel' && state.snapshots) {
				my_time_status.present_freeze = my_present
				const my_snapshot = get_snapshot_with_hash(state.snapshots, state.time_status.viewed_snapshot)
				my_present = my_snapshot.state
			}

			return Object.assign({}, state, {present: my_present, time_status: my_time_status})
		}
		return state
	},

	[REPLACE_CURRENT]: (state, action) => {
		if (!(state.present && state.snapshots)) {
			return state
		}


		const new_snapshots = state.snapshots.map(new_snapshot => {
				if (new_snapshot.id === state.current_snapshot) {
					return {
						id: new_snapshot.id,
						state: state.present
					}
				}
				else {
					return new_snapshot
				}
			}
		)

		return Object.assign({}, state, {snapshots: new_snapshots})

	},

	[INSERT_BETWEEN_CURRENT_AND_CHILDREN]: (state, action) => {
		if (!(state.present && state.snapshots && state.counter)) {
			return state
		}

		const payload = action.payload
		const hash_id = state.counter + 1
		const new_snapshots = [
			...state.snapshots,
			{
				id: hash_id,
				state: state.present
			}
		]

		let new_histories = []
		let new_snapshot_siblings = [hash_id]
		let new_parents_children = []  //===================

		if (state.history_frames) {
			new_histories = state.history_frames.map(history_frame => {
						if (history_frame.id === state.current_snapshot) {
							new_parents_children = history_frame.children
							return Object.assign({}, history_frame, {children: [hash_id]})
						}else if(history_frame.parent === state.current_snapshot ){
							return Object.assign({}, history_frame, {parent: hash_id})
						}
						else {
							return history_frame
						}
					}
			)
		}

		new_histories = [
			...new_histories,
			{
				id: hash_id,
				description: payload.description,
				children: new_parents_children,
				parent: state.current_snapshot
			}
		]

		return Object.assign({}, state, {
			snapshots: new_snapshots,
			history_frames: new_histories,
			current_snapshot: hash_id,
			counter: hash_id,
			time_status: {
				mode: 'present',
				viewed_snapshot: hash_id,
				parent: state.current_snapshot,
				children: new_parents_children,
				snapshot_siblings: new_snapshot_siblings,
				present_freeze: ''
			}
		})

	},

	[DELETE_CURRENT_SNAPSHOT]: (state, action) => {
		if (!(state.present && state.snapshots)) {
			return state
		}

		const new_snapshots = state.snapshots.filter(new_snapshot => {
					return !(new_snapshot.id === state.current_snapshot)
				}
		)

		let new_histories = state.history_frames
		let my_history_frame = false

		if (state.history_frames) {
			new_histories = new_histories.filter(history_frame => {
								if(history_frame.id === state.current_snapshot) {
									my_history_frame = history_frame
									return false
								}
								return true
						}
			)


			if(my_history_frame){
				let parent_frame = false
				new_histories = new_histories.map(history_frame => {
						if(history_frame.id === my_history_frame.parent ){
							let parent_children = history_frame.children.filter(item =>{
								return item != my_history_frame.id
							})
							//parent_children = [...parent_children, my_history_frame.children]
							parent_children = _.concat(parent_children, my_history_frame.children)
							parent_frame = Object.assign({}, history_frame, {children: parent_children})
							return parent_frame
						}else if(history_frame.parent === my_history_frame.id ){
							let children_parent = my_history_frame.parent
							return Object.assign({}, history_frame, {parent: children_parent})
						}
						else {
							return history_frame
						}
					}
				)

				let new_time_status = state.time_status

				//use parent snapshot as new present
				let parent_snapshot = ''
				parent_snapshot = get_snapshot_with_hash (new_snapshots, my_history_frame.parent)

				if(new_time_status.viewed_snapshot == my_history_frame.id
					&& parent_frame && parent_snapshot !== ''){
					new_time_status = Object.assign({}, new_time_status, {
						viewed_snapshot: parent_frame.id,
						parent: parent_frame.parent,
						children: parent_frame.children
					})

					return Object.assign({}, state, {
						snapshots: new_snapshots,
						time_status: new_time_status,
						history_frames: new_histories,
						current_snapshot: my_history_frame.parent,
						present: parent_snapshot.state
					})
				}


			}
		}


		return state

	},

	//todo figure out a way to handle deleting the first node.
	[DELETE_CURRENT_AND_CHILDREN]: (state, action) => {
		if (!(state.present && state.snapshots && state.history_frames)) {
			return state
		}

		if (state.history_frames) {
			let new_histories = state.history_frames
			let removed_ids = [state.current_snapshot]
			let my_history_frame = false
			let parent_frame = false

			new_histories = new_histories.filter(history_frame => {
						if (history_frame.id === state.current_snapshot) {
							my_history_frame = history_frame
							removed_ids = _.concat(removed_ids, my_history_frame.children)
							return false
						}
						return true
					}
			)


			if (!my_history_frame) {
				return state
			}

			if (my_history_frame.parent !== 0) {
				new_histories = new_histories.map(history_frame => {
												if (history_frame.id === my_history_frame.parent) {
													//parent_frame = history_frame
													let tmp_children = history_frame.children.filter(child =>{
														return child != my_history_frame.id
													})
													parent_frame = Object.assign({}, history_frame, {children: tmp_children})
													return parent_frame
												}
												return history_frame
											}
									)
			}else{
				return state  //Cannot delete the root node
			}

			let tmp_removed_arr = my_history_frame.children
			let new_tmp_removed_arr = []
			while (tmp_removed_arr && tmp_removed_arr.length >= 1) {
				new_tmp_removed_arr = []
				new_histories = new_histories.filter(history_frame => {
							if (tmp_removed_arr.indexOf(history_frame.id) != -1) {
								new_tmp_removed_arr = _.concat(new_tmp_removed_arr, history_frame.children)
								removed_ids = _.concat(removed_ids, history_frame.children)
								return false
							}
							return true
						}
				)
				tmp_removed_arr = new_tmp_removed_arr
			}


			const new_snapshots = state.snapshots.filter(new_snapshot => {
						return (removed_ids.indexOf(new_snapshot.id) == -1)
					}
			)

			let new_time_status = state.time_status
			//use parent snapshot as new present
			let parent_snapshot = ''
			parent_snapshot = get_snapshot_with_hash (new_snapshots, my_history_frame.parent)

			if (new_time_status.viewed_snapshot == my_history_frame.id
				&& parent_frame && parent_snapshot) {
				new_time_status = Object.assign({}, new_time_status, {
					viewed_snapshot: parent_frame.id,
					parent: parent_frame.parent,
					children: parent_frame.children
				})
			}else{
				return state
			}

			return Object.assign({}, state, {
				snapshots: new_snapshots,
				time_status: new_time_status,
				history_frames: new_histories,
				current_snapshot: my_history_frame.parent,
				present: parent_snapshot.state
			})

		}
		return state

	},

	[SERVER_FETCH]: (state, action) => {
		//return state
		if (action.payload.my_state) {
			return action.payload.my_state
		}
		return state
	},

	[UPDATE_INPUTS_FROM_TIMELINE]: (state, action) => {
		if(state.present.inputs){
			const new_present = Object.assign({}, state.present, {inputs: action.payload.inputs})
			return Object.assign({}, state, {present: new_present})
		}
		return state
	},

	[DELETE_INPUTS_LOCAL_FROM_TIMELINE]: (state, action) => {
		if(action.payload.uid && state.present.inputs){
			//return Object.assign({}, state, {inputs: action.payload.inputs})
			const my_uid = action.payload.uid
			const new_inputs = state.present.inputs.filter(input => {
						return !(input.id === my_uid)
					}
			)
			const new_present = Object.assign({}, state.present, {inputs: new_inputs})
			return Object.assign({}, state, {present: new_present})
		}
		return state
	},

	[CLEAN_TIME_PAD_IN_PRESENT]: (state, action) => {
		const latest_past = [...state.past, state.present]
		const clean_present = {inputs: [], node_title: 'no node title', node_title_editable: 'no'}
		const new_time_statue = Object.assign({}, state.time_status, {mode: 'present'})
		return Object.assign({}, state, {
			past: latest_past,
			time_status: new_time_statue,
			present: clean_present
		})
	},

	//Use `time_travel` and `demo`
	[SET_DEMO_MODE]: (state, action) => {
		if(action.payload.mode){
			return Object.assign({}, state, {demo_mode: action.payload.mode})
		}

		return state
	}


}


// ------------------------------------
// Reducer
// ------------------------------------



/**
 * TimeLineReducer is Reducer Enhancer that can be used to track timeline of events
 */
export default function  timeLineEnhancer (undoable) {

	//const {past, present, future} = undoable(undefined, {})
	const undoable_or_reducer_states = undoable(undefined, {})
	const initialState_part1 = {
		snapshots: [],
		history_frames: [],
		current_snapshot: 0,
		counter: 0,
		time_status: {
			mode: 'present',
			viewed_snapshot: 0,
			parent: '',
			children: [],
			snapshot_siblings: [],
			present_freeze: ''  //no need to freeze present since it is already in "present" mode
		},
		file_spinner: 'active',
		demo_mode: 'off'
	}

	const initialState = Object.assign({}, initialState_part1, undoable_or_reducer_states)

	return function (state = initialState, action) {
		const { snapshots, history_frames, current_snapshot,
			      time_status, past, present, future } = state

		const initial_undoable_state = { past, present, future }

		const handler = ACTION_HANDLERS[action.type]
		let updated_state = {}

		if(handler){
			updated_state = handler(state, action)
		}else{
			updated_state = undoable(initial_undoable_state, action)
		}

		return Object.assign({}, state, updated_state)
	}

}


//export default timeLineEnhancer


// --------------------------------------------
// Helper functions
// --------------------------------------------
export function request_prev_history_frame(state, sizes=1) {
	let parent_frame = ''
	let i = 0
	let parent_hash = state.time_status.parent
	while(i < sizes && parent_hash){
		for (var history_frame of state.history_frames) {
			if (history_frame.id == parent_hash) {
				parent_frame = history_frame
				parent_hash = history_frame.parent
				break;
			}
		}
		i += 1
	}

	return parent_frame;
}


export function request_next_history_frame(state, sizes=1) {
	let parent_frame = 'no-child'
	let i = 0
	//assuming there is only one child of the current snapshot to begin with
	let children_hash = state.time_status.children
	if(children_hash.length > 1){
		//indicating users have to choose one of the children(future) first
		return 'multiple'
	}

	while(i < sizes && children_hash.length == 1){
		for (var history_frame of state.history_frames) {
			if (history_frame.id == children_hash[0]) {
				parent_frame = history_frame
				children_hash = history_frame.children
				break;
			}
		}
		i += 1
	}

	return parent_frame;
}


export function get_snapshot_with_hash (snapshots, my_hash) {
	let returned_snapshot = ''
	if(snapshots){
		for (var snapshot of snapshots) {
			if (snapshot.id == my_hash) {
				returned_snapshot = snapshot
				break
			}
		}
	}
	return returned_snapshot
}

export function only_freeze_time_from_present_to_time_travel(state) {
	//Only freeze "present" when it transitions from "present" to "time_travel"
	let present_freeze = state.time_status.present_freeze
	if (state.time_status.mode && state.time_status.mode == 'present') {
		present_freeze = state.present
	}
	return present_freeze;
}


export function get_history_frame_with_hash(history_frames, chosen_hash) {
	let my_history_frame = false
	for (var history_frame of history_frames) {
		if (history_frame.id == chosen_hash) {
			my_history_frame = history_frame
			break;
		}
	}
	return my_history_frame;
}