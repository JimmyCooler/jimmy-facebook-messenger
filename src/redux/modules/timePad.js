// ----------------------------------
// Dependencies
// ----------------------------------
const marked = require('marked')




// ------------------------------------
// Constants
// ------------------------------------
export const NOTE_INCREMENT = 'NOTE_INCREMENT'
export const SAVE_INPUT_BOX = 'SAVE_INPUT_BOX'
export const CHOOSE_OUTPUT_FORM = 'CHOOSE_OUTPUT_FORM'
export const SAVE_INPUT_PERIODICALLY = 'SAVE_INPUT_PERIODICALLY'
export const UPDATE_INPUTS = 'UPDATE_INPUTS'
export const DELETE_INPUTS = 'DELETE_INPUTS'
export const DELETE_INPUTS_LOCAL = 'DELETE_INPUTS_LOCAL'
export const UPDATE_SNAPSHOT = 'UPDATE_SNAPSHOT'
export const COLLAPSE_INPUT = 'COLLAPSE_INPUT'
export const SORT_INPUTS = 'SORT_INPUTS'
export const SET_ITEM_TITLE_EDITABLE = 'SET_ITEM_TITLE_EDITABLE'
export const CHANGE_TITLE_CONTENT = 'CHANGE_TITLE_CONTENT'
export const UPDATE_NODE_TITLE = 'UPDATE_NODE_TITLE'
export const SET_NODE_TITLE_EDITABLE = 'SET_NODE_TITLE_EDITABLE'


// ------------------------------------
// Actions
// ------------------------------------
export function increment (index): Action {
	return {
		type: NOTE_INCREMENT,
		payload: index
	}
}


export function saveInputBox (index: number, input_text: string=''): Action {
	return {
		type: SAVE_INPUT_BOX,
		payload: {
			id: index,
			text: input_text,
			output: input_text
		}
	}
}

export function chooseOutputFormat (index: number, output_format: string='md'): Action {
	return {
		type: CHOOSE_OUTPUT_FORM,
		payload: {
			id: index,
			format: output_format
		}
	}
}

export function saveInputPeriodically (index: number, input_text: string=''): Action {
	return {
		type: SAVE_INPUT_PERIODICALLY,
		payload: {
			id: index,
			text: input_text,
			output: input_text
		}
	}
}



export function update_inputs (inputs) {
	return {
		type: UPDATE_INPUTS,
		payload: {
			inputs: inputs
		}
	}
}

export function collapse_input (is_collapse, input_id) {
	return {
		type: COLLAPSE_INPUT,
		payload: {
			is_collapse: is_collapse,
			input_id: input_id
		}
	}
}

export function sort_inputs (input_id, new_position) {
	return {
		type: SORT_INPUTS,
		payload: {
			input_id: input_id,
			new_position: new_position
		}
	}
}

export function set_item_title_editable (input_id, is_editable) {
	return {
		type: SET_ITEM_TITLE_EDITABLE,
		payload:{
			input_id: input_id,
			is_editable: is_editable
		}
	}
}


export function change_title_content (input_id, title_content) {
	return {
		type: CHANGE_TITLE_CONTENT,
		payload:{
			input_id: input_id,
			title_content: title_content
		}
	}
}


export function update_item_title_and_hide_editable( input_id, title_content ) {
	return (dispatch) => {
		dispatch( change_title_content (input_id, title_content) )
		dispatch(set_item_title_editable (input_id, "no"))
	}
}


export function update_node_title(title_content){
	return {
		type: UPDATE_NODE_TITLE,
		payload:{
			title_content: title_content
		}
	}
}


export function set_node_title_editable(is_editable){
	return {
		type: SET_NODE_TITLE_EDITABLE,
		payload:{
			is_editable: is_editable
		}
	}
}

export function update_node_title_and_hide_editable( title_content ) {
	return (dispatch) => {
		dispatch( update_node_title(title_content) )
		dispatch( set_node_title_editable("no") )
	}
}




export const actions = {
	increment,
	saveInputBox,
	chooseOutputFormat,
	saveInputPeriodically,
	update_inputs,
	collapse_input,
	sort_inputs,
	set_item_title_editable,
	change_title_content,
	update_item_title_and_hide_editable,
	update_node_title,
	set_node_title_editable,
	update_node_title_and_hide_editable
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
	[NOTE_INCREMENT]: (state, action) => {
		const new_inputs = [
			...state.inputs,
			{
				id: action.payload,
				text: '',
				output: '',
				format: 'plain',
				is_collapse: 'no',
				title: 'no title',
				update_title: 'no'
			}
		]

		return Object.assign({}, state, {inputs: new_inputs})
	},

	[SAVE_INPUT_BOX]: (state, action) => {
		const new_inputs = state.inputs.map(input => {
				if( input.id === action.payload.id ){
					let new_output = action.payload.text
					 if(input.format === 'md'){
					    new_output = marked(action.payload.text)
					 }

					return Object.assign({}, input, {text: action.payload.text, output: new_output})
				}else{
					return input
				}
			}
		)

		return Object.assign({}, state, {inputs: new_inputs})
	},

	[CHOOSE_OUTPUT_FORM]: (state, action) => {

		const new_inputs = state.inputs.map(input => {
				if(input.id === action.payload.id){
					return Object.assign({}, input, {
						format: action.payload.format
					})
				}else{
					return input
				}
		})

		return Object.assign({}, state, {inputs: new_inputs})
	},

	[SAVE_INPUT_PERIODICALLY]: (state, action) => {
		const new_inputs = state.inputs.map(input => {
					if( input.id === action.payload.id ){
						let new_output = action.payload.text
						if(input.format === 'md'){
							new_output = marked(action.payload.text)
						}

						return Object.assign({}, input, {text: action.payload.text, output: new_output})
					}else{
						return input
					}
				}
		)

		return Object.assign({}, state, {inputs: new_inputs})
	},

	[UPDATE_INPUTS]: (state, action) => {

		if(action.payload.inputs){
			return Object.assign({}, state, {inputs: action.payload.inputs})
		}
		return state
	},

	[DELETE_INPUTS_LOCAL]: (state, action) => {
		if(action.payload.uid && state.inputs){
			//return Object.assign({}, state, {inputs: action.payload.inputs})
			const my_uid = action.payload.uid
			const new_inputs = state.inputs.filter(input => {
						return !(input.id === my_uid)
					}
			)
			return Object.assign({}, state, {inputs: new_inputs})
		}
		return state
	},

	[COLLAPSE_INPUT]: (state, action) => {
		console.log("can u see me at 213==>", action.payload.input_id);
		if(action.payload.is_collapse && action.payload.input_id){
			const new_inputs = state.inputs.map(input => {
				console.log(input.id, "<=input_id  payload_id=>", action.payload.input_id)
					if( input.id === action.payload.input_id ){
						const result =  Object.assign({}, input, {is_collapse: action.payload.is_collapse})
						return result
					}else{
						return input
					}
				}
			)


			return Object.assign({}, state, {inputs: new_inputs})
		}
		return state
	},

	[SORT_INPUTS]: (state, action) => {
		if(action.payload.input_id && action.payload.new_position){
			let input_in_dest = ''
			let input_in_origin = ''
			const check_in_origin = state.inputs.map( (input, index) => {
						if( input.id === action.payload.input_id ){
							input_in_origin = input
							return true
						}else{
							return false
						}
					}
			)

			/*console.log("the new position is ", action.payload.new_position)

			console.log("the input in origin is ")
			console.log(input_in_origin)*/

			const check_in_dest = state.inputs.map( (input, index) => {
				    if( index == action.payload.new_position ){
							input_in_dest = input
							return true
						}else{
							return false
						}
					}
			)


			if(check_in_origin && check_in_dest){
				const new_inputs = state.inputs.map( (input, index) => {
					if( input.id == action.payload.input_id ){
						return input_in_dest
					}else if( index == action.payload.new_position ){
						return input_in_origin
					}else{
						return input
					}
				})


				return Object.assign({}, state, {inputs: new_inputs})
			}

		}
		return state
	},


	[SET_ITEM_TITLE_EDITABLE]: (state, action) => {
		if (action.payload.input_id && action.payload.is_editable) {
			const new_inputs = state.inputs.map( (input, index) => {
				if( input.id == action.payload.input_id ){
					return Object.assign({}, input, {update_title: action.payload.is_editable})
				}else{
					return input
				}
			})
			
			return Object.assign({}, state, {inputs: new_inputs})
		}
		return state
	},

	[CHANGE_TITLE_CONTENT]: (state, action) => {
		if (action.payload.input_id && action.payload.title_content) {
			const new_inputs = state.inputs.map( (input, index) => {
				if( input.id == action.payload.input_id ){
					return Object.assign({}, input, {title: action.payload.title_content})
				}else{
					return input
				}
			})

			return Object.assign({}, state, {inputs: new_inputs})
		}
		return state
	},


	[UPDATE_NODE_TITLE]: (state, action) => {
		if ( action.payload.title_content ) {
			return Object.assign({}, state, {node_title: action.payload.title_content})
		}
		return state
	},

	[SET_NODE_TITLE_EDITABLE]: (state, action) => {
		if (action.payload.is_editable) {
			return Object.assign({}, state, {node_title_editable: action.payload.is_editable})
		}
		return state
	}


}



// ------------------------------------
// Reducer
// ------------------------------------

const initialState = {inputs: [], node_title: 'no node title', node_title_editable: 'no'}
export default function timePadReducer (state: total_notes = initialState, action: Action) {
	const handler = ACTION_HANDLERS[action.type]

	return handler ? handler(state, action) : state
}
