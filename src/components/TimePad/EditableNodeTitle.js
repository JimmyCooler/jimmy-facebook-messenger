import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import classes from '../../views/TimePadView/TimePadView.scss'
import classname from 'classname'


class EditableNodeTitle extends Component{
	constructor(props) {
		super(props)
	}

	static propTypes = {
		set_node_title_editable: PropTypes.func.isRequired,
		update_node_title_and_hide_editable: PropTypes.func.isRequired,
		node_title: PropTypes.string.isRequired,
		node_title_editable: PropTypes.string.isRequired
	};

	render () {
		const title_input_div = <input
				onKeyDown={
									(event)=>{
										if (event.keyCode == 13){
												this.props.update_node_title_and_hide_editable(event.target.value)
										}
									}
								}
				type="text" className="form-control" id="item_title" placeholder="Enter Your Item Title"
				defaultValue={this.props.node_title?this.props.node_title:''}
				onFocus={(event)=>{
														const dom = event.target
														jQuery(dom).putCursorAtEnd()
												}}
				aria-describedby="basic-addon1" />
		let div_title = ''
		if(this.props.node_title_editable && this.props.node_title_editable == 'yes'){
			div_title = <div className="input-group">
										<span className="input-group-addon" id="basic-addon1">
											Node Title
										</span>
										{title_input_div}
									</div>
		}else{
			let title_text = 'No Title from local'
			if(this.props.node_title){
				title_text = this.props.node_title
			}
			div_title = <div className={classname(classes["node_title_block"])}
											 onClick={() => {this.props.set_node_title_editable('yes')}}>
										{title_text}
									</div>
		}

		return div_title
	}
}





export default EditableNodeTitle
