import React, { PropTypes } from 'react'
import LinkedInputBox from '../../containers/TimePad/LinkedInputBox'
import FileInputBox from '../../containers/TimePad/FileInputBox'


const InputBoxList = ({ inputs, boxChangeEvent,
		update_item_title_and_hide_editable,
		chooseOutputFormat, saveInputPeriodically, collapse_input,
		delete_file_for_doc, delete_input, delete_input_local,
	  set_item_title_editable, sort_inputs}) => {

			const input_size = _.size(inputs)
			return (
					<div>
						{inputs.map( (input, index) =>{
									if(input.format === 'file'){
										return(
												<FileInputBox
																			key={index} input={input}
																			collapse_input={collapse_input}
																			delete_input={delete_input}
																			delete_input_local={delete_input_local}
																			delete_file_for_doc={delete_file_for_doc}
																			input_size = {input_size}
																			sort_inputs = {sort_inputs}
																			set_item_title_editable={set_item_title_editable}
																			index={index} />
										)
									}else{
										return (
												<LinkedInputBox
																				key={index} input={input}
																				collapse_input={collapse_input}
																				delete_input_local={delete_input_local}
																				index={index}
																				chooseOutputFormat={chooseOutputFormat}
																				saveInputPeriodically={saveInputPeriodically}
																				input_size = {input_size}
																				sort_inputs = {sort_inputs}
																				set_item_title_editable={set_item_title_editable}
																				update_item_title_and_hide_editable={update_item_title_and_hide_editable}
																				boxChangeEvent={boxChangeEvent} />
										)
									}
								}
						)}
					</div>
			)
	}







InputBoxList.propTypes = {
	inputs: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string.isRequired,
		format: PropTypes.string.isRequired
	}).isRequired).isRequired
}

export default InputBoxList
