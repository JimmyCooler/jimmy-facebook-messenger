import React, { Component, PropTypes } from 'react'
import InputBoxList from './InputBoxList'
import EditableNodeTitle from './EditableNodeTitle'
import NavArrowMenu from './NavArrowMenu'
import classname from 'classname'
import classes from '../../views/TimePadView/TimePadView.scss'
import {random_hash} from '../../concerns/helpers'
import fetch from 'isomorphic-fetch'

class App extends Component{
	constructor(props) {
		super(props)
		this.save_snapshot = this.save_snapshot.bind(this)
		this.past_snapshot_by_step = this.past_snapshot_by_step.bind(this)
		this.future_snapshot_by_step = this.future_snapshot_by_step.bind(this)
		this.handle_node_click = this.handle_node_click.bind(this)
		this.present_past_switch_handler = this.present_past_switch_handler.bind(this)
		this.set_visited_snapshot_as_current = this.set_visited_snapshot_as_current.bind(this)
		this.save_snapshot_by_insert = this.save_snapshot_by_insert.bind(this)
		this.delete_current_node_handler = this.delete_current_node_handler.bind(this)
		this.delete_current_n_children_handler = this.delete_current_n_children_handler.bind(this)
		this.push_change_to_server_local = this.push_change_to_server_local.bind(this)
		this.upload_file_handler = this.upload_file_handler.bind(this)
		this.alter_nodes_graph = ''
	}

	save_snapshot(){
		main_module.actions.save = 'yes'
		main_module.ready_to_push = 'yes'
		this.alter_nodes_graph = 'yes'
		this.props.save_snapshot( 'descript' )
	}

	save_snapshot_by_insert(){
		main_module.actions.save = 'yes'
		main_module.ready_to_push = 'yes'
		this.alter_nodes_graph = 'yes'
		this.props.insert_between_current_and_children( )
	}

	past_snapshot_by_step(){
		main_module.actions.snapshot_by_step = 'yes'
		this.props.past_snapshot_by_step()
	}

	future_snapshot_by_step(){
		main_module.actions.snapshot_by_step = 'yes'
		this.props.future_snapshot_by_step()
	}

	handle_node_click(event){
		const my_hash = event.target.getAttribute('data-hash')
		this.props.choose_current_snapshot( parseInt(my_hash) )
	}

	present_past_switch_handler(event){
		const tense = event.target.getAttribute('data-tense')
		this.props.present_past_switch(tense)
	}

	set_visited_snapshot_as_current(){
		main_module.actions.set_present_snapshot = 'yes'
		this.props.change_past_from_present(this.props.time_status.viewed_snapshot)
	}

	delete_current_node_handler(){
		main_module.actions.save = 'yes'
		main_module.ready_to_push = 'yes'
		this.alter_nodes_graph = 'yes'
		this.props.delete_current_snapshot()
	}

	delete_current_n_children_handler(){
		main_module.actions.save = 'yes'
		main_module.ready_to_push = 'yes'
		this.alter_nodes_graph = 'yes'
		this.props.delete_current_and_children()
	}

	push_change_to_server_local(){
		if(main_module.slug_id != ''
			&& main_module.ready_to_push == 'yes'){
			const state_doc = {
										is_public: false,
										url_slug:  main_module.slug_id
									}
			this.props.push_change_to_server( state_doc )
			main_module.ready_to_push = 'no'
		}
	}

	test_deleting_file(){
		console.log("can you see me now at 93 - test_deleting_file")
		this.props.delete_input('123')
	}

	upload_file_handler(){
		main_module.ready_to_push = 'yes'
		this.props.upload_file_for_doc('#upload_file', random_hash())
	}


	replace_current_handler(){
		main_module.ready_to_push = 'yes'
		this.props.replace_current()
	}

	componentDidMount(){
		if(main_module.slug_id != ''){
			this.props.pull_change_from_server( main_module.slug_id )
			main_module.actions.save = 'yes'
			this.alter_nodes_graph = 'yes'
		}
	}

	componentWillUpdate(nextProps, nextState){
	}

	componentDidUpdate(nextProps, nextState){
		if(this.alter_nodes_graph == 'yes'){
			document.getElementById("loading_drawing").style.display = 'block';
			this.alter_nodes_graph = ''
			this.push_change_to_server_local()
		}else if(main_module.ready_to_push == 'yes'){
			this.push_change_to_server_local()
		}

	}



	//=========================================

	static propTypes = {
		inputs: PropTypes.array.isRequired,
		my_state: PropTypes.object.isRequired,
		file_spinner: PropTypes.string.isRequired,
		demo_mode: PropTypes.string.isRequired,

		current_snapshot: PropTypes.number.isRequired,
		increment: PropTypes.func.isRequired,
		boxChangeEvent: PropTypes.func.isRequired,
		chooseOutputFormat: PropTypes.func.isRequired,
		saveInputPeriodically: PropTypes.func.isRequired,
		undo: PropTypes.func.isRequired,
		redo: PropTypes.func.isRequired,
		//update_inputs: PropTypes.func.isRequired,
		save_snapshot: PropTypes.func.isRequired,
		past_snapshot_by_step: PropTypes.func.isRequired,
		future_snapshot_by_step: PropTypes.func.isRequired,
		choose_next_future: PropTypes.func.isRequired,
		change_past_from_present: PropTypes.func.isRequired,
		get_snapshot_with_hash: PropTypes.func.isRequired,
		choose_current_snapshot: PropTypes.func.isRequired,
		time_status: PropTypes.object.isRequired,
		present_past_switch: PropTypes.func.isRequired,
		replace_current: PropTypes.func.isRequired,
		insert_between_current_and_children: PropTypes.func.isRequired,
		delete_current_snapshot: PropTypes.func.isRequired,
		pull_change_from_server: PropTypes.func.isRequired,
		push_change_to_server: PropTypes.func.isRequired,
		upload_file_for_doc: PropTypes.func.isRequired,
		delete_file_for_doc: PropTypes.func.isRequired,
		delete_input_local: PropTypes.func.isRequired,
		collapse_input: PropTypes.func.isRequired,
		sort_inputs: PropTypes.func.isRequired,
		set_item_title_editable: PropTypes.func.isRequired,
		update_item_title_and_hide_editable: PropTypes.func.isRequired,
		update_node_title: PropTypes.func.isRequired,
		set_node_title_editable: PropTypes.func.isRequired,
		update_node_title_and_hide_editable: PropTypes.func.isRequired,
		node_title: PropTypes.string.isRequired,
		node_title_editable: PropTypes.string.isRequired,
		clean_time_pad_in_present: PropTypes.func.isRequired,
		set_demo_mode: PropTypes.func.isRequired
	};

	render (){
	//todo: After the backend code is written, "width" and "height" should be determined
		const time_status_current = this.props.current_snapshot || 'N/A'
		const time_status_visiting = this.props.time_status.viewed_snapshot || 'N/A'
		const next = this.future_snapshot_by_step
		const prev = this.past_snapshot_by_step

		let file_spiner
		if( this.props.file_spinner == 'active'){
			file_spiner = <div id="loading_drawing2" className={classname(classes['spinner_loader_for_doc'])}>
											<i className={classname("fa", "fa-spinner", "fa-spin")}></i>
										</div>
		}else{
			file_spiner = ''
		}

		const style1 = {
			border: '1px solid red'
		}


		//============================================
		//let right_colum_content = ''
		if(this.props.demo_mode && this.props.demo_mode == 'on'){
			const my_style={display: 'none'}


			return (
					<div className={classname('container', 'text-center',  classes['main-container'])}>

						<button type="button" className={classname("btn", "btn-success", classes['demo_button_off'])}
										onClick={() => this.props.set_demo_mode('off')}>Demo On</button>

						<NavArrowMenu past_snapshot_by_step={this.past_snapshot_by_step}
													future_snapshot_by_step={this.future_snapshot_by_step}	/>

						<div className='row'>
							<div className="col-md-12">

								<div id="left-column" style={my_style}
										 className={classname("col-md-3", "col-sm-3", "col-xs-3", "left-column")}>
									<div className={classname(classes['status_bar'])}>
										<div>
											Current ID: <span className="badge" id="current_position">{time_status_current}</span>

										</div>
										<div>Visiting ID: <span className="badge" id="visiting_position">{time_status_visiting}</span></div>
										<div>Mode: <span className="badge">{this.props.time_status.mode}</span></div>
										<div id="loading_drawing" className={classname(classes['spinner_loader'])}>
											<i className={classname("fa", "fa-gear", "fa-spin")}></i>
										</div>
									</div>
									<div className={classname(classes['graphWrapper'])}>
										<canvas id="TimeLineGraph" resize width="2000" height="2000" className={classname(classes['timePadNodes'])}></canvas>
									</div>
								</div>

								<div className="col-md-12 col-sm-12 col-xs-12">
									<span id="tell_react"  value=""  onClick={this.handle_node_click}/>
									<h1 onClick={() => this.props.set_demo_mode('off')}>This is the demo time</h1>

								</div>

							</div>
						</div>
					</div>

			)
		}
		//==========================================================



		return(

				<div className={classname('container', 'text-center',  classes['main-container'])}>
					<button type="button" className={classname("btn", "btn-default", classes['demo_button_on'])}
									onClick={() => this.props.set_demo_mode('on')}>Demo Off</button>


					<h1>Welcome to Time Lord's TimePad</h1>
					<div className='row'>
						<div className="col-md-12">
							<div id="left-column" className={classname("col-md-3", "col-sm-3", "col-xs-3", "left-column")}>
								<div className={classname(classes['status_bar'])}>
									<div>
										Current ID: <span className="badge" id="current_position">{time_status_current}</span>

									</div>
									<div>Visiting ID: <span className="badge" id="visiting_position">{time_status_visiting}</span></div>
									<div>Mode: <span className="badge">{this.props.time_status.mode}</span></div>
									<div id="loading_drawing" className={classname(classes['spinner_loader'])}>
										<i className={classname("fa", "fa-gear", "fa-spin")}></i>
									</div>
								</div>
								<div className={classname(classes['graphWrapper'])}>
									<canvas id="TimeLineGraph" resize width="2000" height="2000" className={classname(classes['timePadNodes'])}></canvas>
								</div>

							</div>

							<div className="col-md-9 col-sm-9 col-xs-9">
								<div className={classname(classes['menu_nav_block'])}>
									<div className="btn-group" role="group" aria-label="">
										<button type="button" className="btn btn-default btn-xs" onClick={this.test_deleting_file.bind(this)}>Delete</button>
										<button type="button" className="btn btn-default btn-xs" onClick={this.props.undo}>Undo</button>
										<button type="button" className="btn btn-default btn-xs" onClick={this.props.redo}>Redo</button>
										<button type="button" className="btn btn-default btn-xs" onClick={this.past_snapshot_by_step}>Prev</button>
										<button type="button" className="btn btn-default btn-xs" onClick={this.future_snapshot_by_step}>Next</button>

										<div className="dropdown, btn-group">
											<button className={classname(classes['nav_button_list'], "btn", "btn-default", "btn-xs", "dropdown-toggle")}
															type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												Save
												<span className="caret"></span>
											</button>
											<ul className={classname(classes['dropdown-menu-custom'], "dropdown-menu")} aria-labelledby="dropdownMenu1">
												<li><a onClick={this.save_snapshot}>Add New Child</a></li>
												<li><a onClick={this.replace_current_handler.bind(this)}>Replace Current</a></li>
												<li><a onClick={this.save_snapshot_by_insert}>Insert Between Current and Its Children</a></li>
											</ul>
										</div>

										<div className="dropdown, btn-group">
											<button className={classname(classes['nav_button_list'], "btn", "btn-default",  "btn-xs", "dropdown-toggle")}
															type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												Mode
												<span className="caret"></span>
											</button>
											<ul className={classname(classes['dropdown-menu-custom'], "dropdown-menu")} aria-labelledby="dropdownMenu1">
												<li><a onClick={this.present_past_switch_handler} data-tense="present">Back to Present</a></li>
												<li><a onClick={this.present_past_switch_handler} data-tense="time_travel">Back to Past</a></li>
												<li><a onClick={this.set_visited_snapshot_as_current}>Set As Present</a></li>
											</ul>
										</div>

										<div className="dropdown, btn-group">
											<button className={classname(classes['nav_button_list'], "btn", "btn-default",  "btn-xs", "dropdown-toggle")}
															type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												Delete
												<span className="caret"></span>
											</button>
											<ul className={classname(classes['dropdown-menu-custom'], "dropdown-menu")} aria-labelledby="dropdownMenu1">
												<li><a onClick={this.delete_current_node_handler}>Delete Current</a></li>
												<li><a onClick={this.delete_current_n_children_handler}>Delete Current and its children</a></li>
											</ul>
										</div>

										<button type="button" className="btn btn-default btn-xs" onClick={this.props.increment}>Add</button>
										<button type="button" className="btn btn-default btn-xs" onClick={this.props.clean_time_pad_in_present}>Clean</button>
									</div>
								</div>




								<span id="tell_react"  value=""  onClick={this.handle_node_click}/>


								<EditableNodeTitle
										set_node_title_editable={this.props.set_node_title_editable}
										update_node_title_and_hide_editable={this.props.update_node_title_and_hide_editable}
										node_title={this.props.node_title}
										node_title_editable={this.props.node_title_editable}
								/>

								<InputBoxList inputs={this.props.inputs} chooseOutputFormat={this.props.chooseOutputFormat}
															delete_file_for_doc={this.props.delete_file_for_doc}
															saveInputPeriodically={this.props.saveInputPeriodically}
															delete_input={this.props.delete_input}
															delete_input_local={this.props.delete_input_local}
															collapse_input={this.props.collapse_input}
															sort_inputs={this.props.sort_inputs}
															set_item_title_editable={this.props.set_item_title_editable}
															update_item_title_and_hide_editable={this.props.update_item_title_and_hide_editable}
															boxChangeEvent={this.props.boxChangeEvent} />


								<div className={classname(classes['download_bar'], 'col-md-12', 'alert',  'alert-success')}>
									    <input type="file" id="upload_file" onChange={this.upload_file_handler} />
								</div>

								{file_spiner}





							</div>

					  </div>
				  </div>
		    </div>
		)
	}
}


export default App
