import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import classes from '../../views/TimePadView/TimePadView.scss'
import classname from 'classname'


class EditableTitle extends Component{
	constructor(props) {
		super(props)
	}

	static propTypes = {
		update_item_title_and_hide_editable: PropTypes.func.isRequired,
		make_item_editable: PropTypes.func.isRequired,
		index: PropTypes.number.isRequired,
		update_title: PropTypes.string.isRequired,
		save_button: PropTypes.object.isRequired,
	  delete_button: PropTypes.object.isRequired,
	  div_expand_or_collapse: PropTypes.object.isRequired,
	  my_title: PropTypes.string.isRequired,
	  input_id: PropTypes.string.isRequired
	};

	render () {
		const title_input_div = <input
				//onChange={(event) => {this.input_title = event.target.value}}
				onKeyDown={
									(event)=>{
										if (event.keyCode == 13){
												this.props.update_item_title_and_hide_editable(this.props.input_id, event.target.value)
										}
									}
								}
				type="text" className="form-control" id="item_title" placeholder="Enter Your Item Title"
				defaultValue={this.props.my_title?this.props.my_title:''}
				onFocus={(event)=>{
														const dom = event.target
														jQuery(dom).putCursorAtEnd()
												}}
				aria-describedby="basic-addon1" />
		let div_title = ''
		if(this.props.update_title && this.props.update_title == 'yes'){
			div_title = <div className="input-group">
										<span className="input-group-addon" id="basic-addon1">
											Item#{this.props.index + 1}
										</span>

				{title_input_div}
										<span className="input-group-btn">
											{this.props.save_button}
											{this.props.delete_button}
											{this.props.div_expand_or_collapse}
										</span>

			</div>
		}else{
			let title_text = 'No Title'
			if(this.props.my_title){
				title_text = this.props.my_title
			}
			div_title = <div className="input-group">
										<span className={classname("input-group-addon", classes['input_addon_customized1'])}>
											Item#{this.props.index + 1}
										</span>

				<div className={classname(classes["title_block"])}
						 onClick={() => {this.props.make_item_editable(this.props.input_id, 'yes')}}>
					{title_text}
				</div>

										<span className="input-group-btn">
											{this.props.delete_button}
											{ this.props.div_expand_or_collapse }
										</span>

			</div>
		}


		return (

				<div
						className={classname("form-group", classes['item_header_block'])}>
					{div_title}
				</div>
		)
	}
}





export default EditableTitle
