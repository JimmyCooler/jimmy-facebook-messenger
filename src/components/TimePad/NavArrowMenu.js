import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import classes from '../../views/TimePadView/TimePadView.scss'
import classname from 'classname'


class NavArrowMenu extends Component{
	constructor(props) {
		super(props)
	}

	static propTypes = {
		past_snapshot_by_step: PropTypes.func.isRequired,
		future_snapshot_by_step: PropTypes.func.isRequired
	};

	//<i className={classname('fa', 'fa-play-circle', classes['nav_center'])} aria-hidden="true"></i>
	//<i className={classname('fa', 'fa-pause-circle', classes['nav_center'])} aria-hidden="true"></i>

	render () {
		  const myStyle={border: "1px solid red;"}
			return (
				<div className={classname(classes['arrow_nav_control'])}>
					<i className={classname('fa', 'fa-arrow-up', classes['nav_up'])}
						 onClick={this.props.past_snapshot_by_step} aria-hidden="true"></i>
					<i className={classname('fa', 'fa-arrow-down',  classes['nav_down'])}
						 onClick={this.props.future_snapshot_by_step} aria-hidden="true"></i>
					<i className={classname('fa', 'fa-arrow-left',  classes['nav_left'])} aria-hidden="true"></i>
					<i className={classname('fa', 'fa-arrow-right', classes['nav_right'])} aria-hidden="true"></i>
					<i className={classname('fa', 'fa-play-circle', classes['nav_center'])} aria-hidden="true"></i>
				</div>
			)
	}
}





export default NavArrowMenu
