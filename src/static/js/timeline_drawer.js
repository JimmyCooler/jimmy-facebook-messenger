//tool._minDistance = 10;
//tool.maxDistance = 45;
//tool.fixedDistance = 30;

function memoize( fn ) {
	return function () {
		var args = Array.prototype.slice.call(arguments),
				hash = "",
				i = args.length;
		currentArg = null;
		while (i--) {
			currentArg = args[i];
			hash += (currentArg === Object(currentArg)) ?
					JSON.stringify(currentArg) : currentArg;
			fn.memoize || (fn.memoize = {});
		}
		return (hash in fn.memoize) ? fn.memoize[hash] :
				fn.memoize[hash] = fn.apply(this, args);
	};
}

function sleepFor( seconds ){
	var sleepDuration = seconds * 1000
	var now = new Date().getTime();
	while(new Date().getTime() < now + sleepDuration){ /* do nothing */ }
}

function Timeline_Drawer(){
	var fix_radius = 10;
	var grid_size = 40;
	var path_length = 50;
	var vector1 = new Point({x:0, y:10});
	var vector2 = vector1.clone();
	vector2.angle = vector2.angle + 45;
	var vector3 = vector1.clone();
	vector3.angle = vector3.angle - 45;

	var all_nodes = [];

	var cached_objects = [];


	var draw_point_in_grid = function(x_level, y_level){
		var x = grid_size / 2 + grid_size * x_level;
		var y = grid_size / 2 + path_length * y_level;
		var point = new Point(x, y);
		return point;
	};

	var draw_node_point = function(node_num) {
		var x = grid_size / 2;
		var y = grid_size / 2 + path_length * node_num;
		var point = new Point(x, y);
		return point
	};

	var draw_vertical_future_line = function(point1, point2) {
		var vector = point1 - point2;
		var length = vector.length;
		var segments = [point1];
		var amount = 20;
		var length_unit = length / amount;
		var tmp_x = point1.x;
		var tmp_y = point1.y;
		for (var i = 0; i <= amount; i++) {
			//path.add(new Point(1, i / amount)*length);
			var angle2 = Math.PI / 2 * (i + 1);
			var y2 = tmp_y + length_unit * i;
			var x2 = tmp_x + length_unit * Math.sin(angle2);
			var tmp_point = new Point(x2, y2);
			segments.push(tmp_point);
		}
		var path = new Path(segments);
		path.strokeColor = 'black';
		return path;
	}


	var draw_node_in_grid = function(x_level, y_level){
		var point = draw_point_in_grid(x_level, y_level);
		var point2 = point - [fix_radius, fix_radius];
		var rectangle = new Rectangle(point2, new Size(fix_radius*2, fix_radius*2));
		var cornerSize = new Size(5, 5);
		var path = new Path.Rectangle(rectangle, cornerSize);
		path.fillColor = 'black';
		//var my_hue = Math.random()*360;
		//path.fillColor.hue = my_hue;
		path.strokeColor = 'black';
		//console.log('drawing node at ', x_level, '--', y_level);
		//console.log(path.position)

		//all_nodes.push(path);
		return path;
	};

	var draw_main_node_in_grid = function(x_level, y_level){
		var point = draw_point_in_grid(x_level, y_level);
		var point2 = point - [fix_radius, fix_radius];
		var rectangle = new Rectangle(point2, new Size(fix_radius*2, fix_radius*2));
		var cornerSize = new Size(5, 5);
		var path = new Path.Rectangle(rectangle, cornerSize);
		path.fillColor = 'green';
		path.strokeColor = 'green';
		return path;
	};

	var draw_main_node_text_in_grid = function(position, text_input){
		var point = draw_point_in_grid(position.x, position.y);
		var text = new PointText({
			point: (point + [0, fix_radius/2]),
			justification: 'center',
			fontSize: 13,
			fillColor: 'red',
			strokeColor: 'red',
			content: text_input
		});

		return text;
	};


	var draw_node_text_in_grid = function(position, text_input){
		var point = draw_point_in_grid(position.x, position.y);
		var text = new PointText({
			point: (point + [0, fix_radius/2]),
			justification: 'center',
			fontSize: 13,
			fillColor: 'white',
			strokeColor: 'white',
			content: text_input
		});

		return text;
	};

	var connect_parallel_future_nodes = function(level1, level2){
		var point1 = draw_point_in_grid(level1.x, level1.y);
		point1 = point1 + [fix_radius, 0];

		var point2 = draw_point_in_grid(level2.x, level2.y);
		point2 = point2 - [fix_radius, 0];

		var vector = point1 - point2;
		var length = vector.length;
		var segments = [point1];
		var amount = 20;
		var length_unit = length / amount;
		var tmp_x = point1.x;
		var tmp_y = point1.y;
		for (var i = 0; i <= amount; i++) {
			//path.add(new Point(1, i / amount)*length);
			var angle2 = Math.PI / 2 * (i + 1);
			var x2 = tmp_x + length_unit * i;
			var y2 = tmp_y + length_unit * Math.sin(angle2);
			var tmp_point = new Point(x2, y2);
			segments.push(tmp_point);
		}
		var path = new Path(segments);
		path.strokeColor = 'black';


		return path;
	};

	var connect_future_node = function(from_level, to_level){
		var tmp1 = draw_node_point(from_level);
		var point1 = tmp1 + [0, fix_radius];
		var tmp2 = draw_node_point(to_level);
		var point2 = tmp2 - [0, fix_radius];

		var path = draw_vertical_future_line(point1, point2);
		return path;
	};

	var node_arrow = function(from_level){
		var tmp_point = draw_node_point(from_level);
		var point1 = tmp_point + [0, fix_radius];
		var point2 = point1 + vector2;
		var path1 = new Path.Line(point1,point2);
		path1.strokeColor = 'black';

		var point3 = point1 + vector3;
		var path2 = new Path.Line(point1,point3);
		path2.strokeColor = 'black';
		return [path1, path2];
	};

	var connect_node = function(from_level, to_level){
		var tmp1 = draw_node_point(from_level);
		var point1 = tmp1 + [0, fix_radius];
		var tmp2 = draw_node_point(to_level);
		var point2 = tmp2 - [0, fix_radius];
		var path = new Path.Line(point1,point2);
		path.strokeColor = 'black';
		return path;
	};

	//todo: Memoization is not enough for this case, it is also necessary to remove redundant draws from the canvas.

	this.draw_graph = function(timeline_structure){
		all_nodes = [];
		var is_current = 'future';
		var y_i = 0;
		var tree = timeline_structure.tree;
		tree.forEach(function(item) {
			if(!(item[0].is_current == null) || is_current == 'present'){
				if(is_current == 'future'){
					is_current = 'present';
				}else if(is_current == 'present'){
					is_current = 'past';
				}
			}
			var max_length = item.length;
			var x_i = 0;
			var min = 0;
			if(is_current != 'future')
			{
				max_length += 1;
				x_i = 1;
				min += 1;
			}

			item.forEach(function(node) {
				var new_node = ''
				if (is_current != 'future' && node["selected"] != undefined) {
					new_node = draw_main_node_in_grid(0, y_i);
					all_nodes = add_nodes(new_node, node.id, all_nodes);

					new_node = draw_main_node_text_in_grid({x: 0, y: y_i}, node.id);
				}
				new_node = draw_node_in_grid(x_i, y_i);
				all_nodes = add_nodes(new_node, node.id, all_nodes);
				draw_node_text_in_grid({x:x_i, y:y_i}, node.id);
				if(x_i<max_length && x_i>min){
					connect_parallel_future_nodes({x:x_i-1, y:y_i}, {x:x_i, y:y_i});
				}

				x_i += 1;
			});

			var first_item = item[0];
			if(y_i > 0){
				if(is_current == 'future' || is_current == 'present'){
					connect_future_node(y_i-1, y_i);
				}else{
					node_arrow(y_i-1);
					connect_node(y_i-1, y_i);
				}
			}

			y_i += 1;
		});

		return all_nodes;
	};


	var add_nodes = function(new_node, hash, all_nodes){
		//add data to node
		new_node.data.hash = hash;
		all_nodes.push(new_node);
		return all_nodes;
	}


}


function Draw_processor(tmp_time_line_drawer){
	time_lines_drawer = tmp_time_line_drawer;
	nodes = null;
	this.update_time = 500;
	var spinner = document.getElementById("loading_drawing");
	var setup_drawing_graph = function(){
		//spinner.style.display = 'block';
		project.clear();
		nodes = time_lines_drawer.draw_graph(main_module.time_lines);
		spinner.style.display = 'none';
	};

	var unset_triggers = function(){
		main_module.react_trigger = 'no';
		main_module.actions.save = '';
		main_module.actions.snapshot_by_step = '';
		main_module.viewed_hash = '';
		main_module.actions.set_present_snapshot = '';
		main_module.current_snapshot = '';
	};

	this.check_drawing = function(){
		setInterval(function(){
			if(main_module.react_trigger=='yes' && main_module.actions.save == 'yes'){
				setup_drawing_graph();
				unset_triggers();
			}

			if(main_module.react_trigger=='yes'
					&& main_module.actions.snapshot_by_step == 'yes' && main_module.viewed_hash != ''){
				//setup_drawing_graph();
				unset_triggers();
			}

			if(main_module.react_trigger=='yes'
					&& main_module.actions.set_present_snapshot == 'yes'
					&& main_module.current_snapshot != ''){
				setup_drawing_graph();
				/*nodes.some(function(node){
					if (node.data.hash == main_module.current_snapshot){
						document.getElementById("current_position").innerHTML = node.data.hash;
						return true;
					}
				});*/
				unset_triggers();
			}

		}, this.update_time);
	}

	this.get_nodes = function(){
		return nodes;
	}
}

var current_position = '';
var drawer = new Draw_processor(new Timeline_Drawer());
drawer.check_drawing();



//project.activeLayer.fillColor = 'black';
//project.activeLayer.strokeColor = 'black';

function onMouseDown(event) {
	var nodes = drawer.get_nodes();
	if(nodes !== null){
		nodes.some(function(node){
			if (node.bounds.contains(event.point)){
				//document.getElementById("visiting_position").innerHTML = node.data.hash;
				document.getElementById("tell_react").setAttribute('data-hash', node.data.hash)
				document.getElementById("tell_react").click();
				return true;
			}
		});
	}

}

function onMouseUp(event) {
}

function onMouseDrag(event){

}