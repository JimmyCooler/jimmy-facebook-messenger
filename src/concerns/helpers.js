//import {_} from 'lodash/core'
var _ = require('lodash');

export function random_hash(length=10) {
	var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

export function get_tree_structure(history_frames, current_children, tree=[], total=0){
	let next_children = []
	var new_history_frames = _.filter(history_frames, function(frame){
		const id_exists = _.some(current_children, function(value){
								if(frame.id == value ){
									if(frame.children.length >= 1){
										next_children = _.concat(next_children, frame.children)
									}
									return true;
								}else{
									return false;
								}
						})

		return ! id_exists;
	})

	//console.log(new_history_frames)

	if(history_frames.length>0){
		tree[ (tree.length) ] = current_children
		return get_tree_structure(new_history_frames, next_children, tree)
	}else{
		return _(tree).reverse().value()
	}
}


export function get_current_timeline(history_frames, current_id, line_arr=[]){
	let prev_parent = ''
	let new_history_frames = _.filter(history_frames, function(frame){
		if(frame.id == current_id){
				prev_parent = frame.parent
				line_arr.push(current_id)
				return false
		}
		return true
	})

	if(prev_parent){
		return get_current_timeline(new_history_frames, prev_parent, line_arr)
	}else{
		return line_arr
	}
}

export function get_timeline_nodes_with_current(history_frames, current_id){
	const max = history_frames.length
	const time_line_arr = get_current_timeline(history_frames, current_id)
	const root = time_line_arr[time_line_arr.length-1]

	const tree_arr = get_tree_structure(history_frames, [root], [])

	const my_size = _.size(tree_arr)
	let output = []
	let head_hash = time_line_arr.shift()
	let past_current = 'no'
	for(var index=0; index<my_size; index++){
		let row_arr = []
		const row = tree_arr[index]
		let item_index = 0
		for(var item of row){
			if(Array.isArray(item) || item == ''){
				continue
			}
			let obj = {id: item}
			if(item == head_hash){
				if(past_current == 'no'){
					if(item_index === 0){
						obj.is_current = 'y'
					}else{
						let tmp_obj = row_arr[0]
						tmp_obj.is_current = 'y'
						row_arr[0] = tmp_obj
					}

					past_current = 'yes'
				}
				obj.selected = 'y'
				head_hash = time_line_arr.shift()
			}

			row_arr.push(obj)
			item_index += 1
		}
		output.push(row_arr)
	}

	output =  {
		tree:	output,
		total: max
	};


	return output
}


//Drag and drop helpers
export function drapStartHandlerHelper(event){
	event.dataTransfer.effectAllowed = 'move';
	// Firefox requires calling dataTransfer.setData
	// for the drag to properly work
	event.dataTransfer.setData("text/html", event.currentTarget);
}





//Drag and Drop helpers above
//========================================================
/*
export function Memoization_Object(){
	var memo = {}
	this.save_output = function(args, function_name, result){
		memo[function_name] = memo[function_name] || {}
		memo[function_name][]
	}
}
*/





