import React, { Component, PropTypes } from 'react'
import classes from '../../views/TimePadView/TimePadView.scss'
import classname from 'classname'


class FileInputBox extends Component{
	constructor(props) {
		super(props);
		this.delete_file_handler = this.delete_file_handler.bind(this)
	}

	move_input_one_up(event){
		const new_position = this.props.index - 1
		this.props.sort_inputs(this.props.input.id, new_position)
	}

	move_input_one_down(event){
		const new_position = this.props.index + 1
		this.props.sort_inputs(this.props.input.id, new_position)
	}

	handleCollapseInput(event){
		const is_collapse = 'yes'
		this.props.collapse_input(is_collapse, this.props.input.id)
	}

	handleExpandInput(event){
		const is_collapse = 'no'
		this.props.collapse_input(is_collapse, this.props.input.id)
	}

	delete_input_item_handler(event){
		const input_id = this.props.input.id
		this.props.delete_input_local( input_id )
	}

	delete_file_handler(event){
		const input_uid = this.props.input.id
		const uid = this.props.input.file_uid
		this.props.delete_file_for_doc(input_uid, uid)
		return event
	}

	handleImageLoaded() {
		//console.log("the image is loaded")
	}

	//Delete input if image failed to load
	//Image-onload action take place after "componentDidMount"
	handleImageErrored() {
		const input_id = this.props.input.id
		main_module.after_removed_files = 'yes'
		this.props.delete_input( input_id )
	}

	//====================================================================
	//Handling Drag N Drop events
	drapStartHandler(event){
		event.dataTransfer.effectAllowed = 'move';

		// Firefox requires calling dataTransfer.setData
		// for the drag to properly work
		event.dataTransfer.setData("text/html", event.currentTarget);

		if(event.target.dataset.uid && event.target.dataset.position){
			main_module.dragged_input_id = event.target.dataset.uid
			main_module.dragged_input_position = event.target.dataset.position
		}
	}

	draggingHanlder(event){
		//console.log("dragging handler is triggered")
	}

	onDropHandler(event){
		event.preventDefault()

		if(main_module.dragged_input_id && main_module.drop_input_position){
			const input_id = main_module.dragged_input_id
			const new_position = main_module.drop_input_position
			main_module.dragged_input_id = ''
			main_module.drop_input_position = ''
			this.props.sort_inputs(input_id, new_position)
		}
	}

	dragoverHandler(event){
		event.preventDefault();

		const dragIndex = main_module.dragged_input_position

		let target_dom = event.target
		const levels_check = 3
		let level_counter = 0
		while(level_counter < levels_check){
			if(target_dom.dataset.uid){
				break;
			}else{
				target_dom = target_dom.parentNode
				level_counter += 1
			}
		}

		if(! target_dom.dataset.uid){
			return
		}
		
		
		if(target_dom.dataset.collapse){
			main_module.drop_input_position = target_dom.dataset.position
			//console.log('the drop_input_position is ' + main_module.drop_input_position)
			const is_collapse = target_dom.dataset.collapse
			if(is_collapse == "yes"){
				//console.log("drag over collapsed")
				target_dom.className = classname(classes['file_bar'], 'col-md-12', 'alert',  'alert-info', classes['input_shadow'])
			}else{
				//console.log("dragover picture now")
				target_dom.className = classname('col-md-12', classes['input_shadow'])
			}

			//event.target.classList.add(classes['inputBoxWrapper'])
		}



	}

	dragLeaveHandler(event){
		event.preventDefault();
		if(event.target.dataset.position){
			//this.hover_position = event.target.dataset.position
			main_module.drop_input_position = event.target.dataset.position
			const is_collapse = event.target.dataset.collapse
			if(is_collapse == "yes"){
				event.target.className = classname(classes['file_bar'], 'col-md-12', 'alert',  'alert-info')
			}else{
				event.target.className = classname('col-md-12')
			}
		}
	}
	//================================================================


	render (){
		const images_formats = ['jpg', 'png', 'gif', 'tif']
		const my_ext = this.props.input.file_type.toLowerCase();
		const is_image = images_formats.some((element, index, array)=>{
				return my_ext == element
		})
		const collapse_div = <button className="btn btn-link" type="button"
																 onClick={() => this.props.collapse_input('yes', this.props.input.id)}>
														Collapse
		                     </button>

		const expand_div = <button className="btn btn-link" type="button"
																 onClick={() => this.props.collapse_input('no', this.props.input.id)}>
												  Expand
											 </button>

		const item_x_div =  <button className="btn btn-link" type="button"
																onClick={() => this.props.delete_input_local( this.props.input.id )}>
														Delete Item
												</button>

		const file_x_div =  <button className="btn btn-link" type="button"
																onClick={() => this.props.delete_file_for_doc(this.props.input.id, this.props.input.file_uid)}>
													Delete File
												</button>


		if(is_image && this.props.input.is_collapse == 'no'){
			return(
				<div className={classname('form-group', classes['inputBoxWrapperForFile'])}
						 onDragStart={this.drapStartHandler.bind(this)}
						 onDrag={this.draggingHanlder.bind(this)}
						 draggable="true"
						 data-collapse="no"
						 data-uid={this.props.input.id}
						 data-position={this.props.index}
						 onDragOver={this.dragoverHandler.bind(this)}
						 onDragLeave={this.dragLeaveHandler.bind(this)}
						 onDrop={this.onDropHandler.bind(this)}>

					<div className={classname("form-group", classes['item_header_block'])}>
						<div
								className="input-group">
										<span className="input-group-addon" id="basic-addon1">
											Item#{this.props.index + 1}
										</span>

										<span className="input-group-btn">
											{collapse_div}
											{item_x_div}
											{file_x_div}
										</span>
						</div>

          </div>



					<div className={classname("form-group")}>
							<div href = "#"
									/* onDragStart={this.drapStartHandler.bind(this)}
									 onDrag={this.draggingHanlder.bind(this)}*/
									 /*draggable="true"*/
									 onDragOver={this.dragoverHandler.bind(this)}
									 onDragLeave={this.dragLeaveHandler.bind(this)}
									 /*onDrop={this.onDropHandler.bind(this)}*/
		
									 className = {classname(classes['customized_thumbnail'], "thumbnail")}>
								<img
										onLoad={this.handleImageLoaded.bind(this)}
										onError={this.handleImageErrored.bind(this)}
										src = {this.props.input.file_url}/>
							</div>
					</div>	
				</div>
			)
	  }else{
			let image_div = ''
			if(is_image){
				image_div = <a href="#" className={classname(classes['close_file_x'], "close")}
											 onClick={this.handleExpandInput.bind(this)}>Expand</a>
			}

			const download_file_div = <button className="btn  btn-link" type="button"
																				onClick={() => {
																					window.open(
																						this.props.input.file_url,
																						'_blank'
																					)
																				}}>
																	<i className={classname("fa", "fa-file", classes['file_icon'])} aria-hidden="true"></i>
																		{this.props.input.file_name}
																</button>



			return(
					<div className={classname(classes['file_bar'], 'form-group', 'alert',  'alert-info')}
							 onDragStart={this.drapStartHandler.bind(this)}
							 onDrag={this.draggingHanlder.bind(this)}
							 draggable="true"
							 data-collapse="yes"
							 data-uid={this.props.input.id}
							 data-position={this.props.index}
							 onDragOver={this.dragoverHandler.bind(this)}
							 onDragLeave={this.dragLeaveHandler.bind(this)}
							 onDrop={this.onDropHandler.bind(this)}>

						<div className={classname("form-group", classes['item_header_block'])}>
							<div
									className="input-group">
								    <div className={classname(classes['file_title_block'])}>
											<div className={classname(classes['file_title_block_index'])}>
														Item#{this.props.index + 1}
											</div>
											{download_file_div}
										</div>

										<span className="input-group-btn">
											{expand_div}
											{item_x_div}
											{file_x_div}
										</span>
							</div>

						</div>




					</div>
			)
		}
	}
}

FileInputBox.propTypes = {
	input: PropTypes.shape({
		id: PropTypes.string.isRequired,
		format: PropTypes.string.isRequire,
		file_type: PropTypes.string.isRequire,
		file_url: PropTypes.string.isRequired,
		file_name: PropTypes.string.isRequired,
		file_uid: PropTypes.string.isRequired
	}).isRequired,
	index: PropTypes.number.isRequired,
	delete_file_for_doc: PropTypes.func.isRequired,
	delete_input: PropTypes.func.isRequired,
	delete_input_local: PropTypes.func.isRequired,
	collapse_input: PropTypes.func.isRequired,
	input_size: PropTypes.number.isRequired
}

export default FileInputBox

