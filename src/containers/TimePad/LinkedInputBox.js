import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import classes from '../../views/TimePadView/TimePadView.scss'
import classname from 'classname'
import EditableTitle from '../../components/TimePad/EditableTitle'
import {drapStartHandlerHelper} from '../../concerns/helpers'

function createMarkup(output) { return {__html: output} }

class LinkedInputBox extends Component{
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this)
		this.showActiveClass = this.showActiveClass.bind(this)
		this.max_changes = 10
		this.changes_done = 0
		this.input_title = this.props.input.title?this.props.input.title:''

	}

	handleChange(event){
		this.changes_done += 1
		this.input_text = event.target.value
		if(this.changes_done > this.max_changes){
			this.props.boxChangeEvent(this.props.input.id, this.input_text)
			this.props.saveInputPeriodically(this.props.input.id, this.input_text)
			this.changes_done = 0
		}else{
			this.props.boxChangeEvent(this.props.input.id, this.input_text)
		}
	}

	

	showActiveClass(format){
		if(this.props.input.format == format){
			return classname('btn', 'btn-success', 'btn-xs', 'active')
		}else{
			return classname('btn', 'btn-success', 'btn-xs')
		}
	}

	//====================================================================
	//Handling Drag N Drop events
	drapStartHandler(event){
		drapStartHandlerHelper(event)
		/*event.dataTransfer.effectAllowed = 'move';

		// Firefox requires calling dataTransfer.setData
		// for the drag to properly work
		event.dataTransfer.setData("text/html", event.currentTarget);*/

		if(event.target.dataset.uid){
			main_module.dragged_input_id = event.target.dataset.uid
			main_module.dragged_input_position = event.target.dataset.position
		}
	}

	draggingHanlder(event){
		//console.log("dragging handler is triggered")
	}

	onDropHandler(event){
		event.preventDefault()

		if(main_module.dragged_input_id && main_module.drop_input_position){
			const input_id = main_module.dragged_input_id
			const new_position = main_module.drop_input_position
			main_module.dragged_input_id = ''
			main_module.drop_input_position = ''
			this.props.sort_inputs(input_id, new_position)
		}
	}

	dragoverHandler(event){
		event.preventDefault();

		const dragIndex = main_module.dragged_input_position

		let target_dom = event.target
		const levels_check = 3
		let level_counter = 0
		while(level_counter < levels_check){
			if(target_dom.dataset.uid){
				break;
			}else{
				target_dom = target_dom.parentNode
				level_counter += 1
			}
		}

		if(! target_dom.dataset.uid){
			return
		}


		let hoverIndex = target_dom.dataset.position

		//const hoverIndex = event.target.dataset.position

		// Don't replace items with themselves
		if (dragIndex === hoverIndex) {
			return
		}

		// Determine rectangle on screen
		const hoverBoundingRect = ReactDOM.findDOMNode(event.target).getBoundingClientRect();

		// Get vertical middle
		const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

		// Determine mouse position
		const clientOffset = $(event.target).offset();

		// Get pixels to the top
		const hoverClientY = clientOffset.y - hoverBoundingRect.top;

		// Only perform the move when the mouse has crossed half of the items height
		// When dragging downwards, only move when the cursor is below 50%
		// When dragging upwards, only move when the cursor is above 50%

		// Dragging downwards
		if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
			return;
		}

		// Dragging upwards
		if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
			return;
		}

		if(target_dom){
			main_module.drop_input_position = target_dom.dataset.position
			target_dom.className = classname('form-group', classes['inputBoxWrapper'], classes['input_shadow'])
		}
	}

	dragLeaveHandler(event){
		event.preventDefault();
		if(event.target.dataset.position){
			//this.hover_position = event.target.dataset.position
			main_module.drop_input_position = event.target.dataset.position
			event.target.className = classname('form-group', classes['inputBoxWrapper'])
		} 
	}
	//================================================================



	render (){
		let tmp_output
		if( this.props.input.format === 'html' || this.props.input.format === 'md'){
			tmp_output = <div className={classname(classes["custom_tmp_output"])}
												dangerouslySetInnerHTML={createMarkup(this.props.input.output)}></div>
		}else{
			tmp_output = <div className={classname(classes["plain_tmp_output"])} >{this.props.input.output}</div>
		}

		//hide elements on collapse
		let div_formats = ''
		let div_area = ''
		let div_output = ''
		let div_expand_or_collapse = <button className="btn btn-link" type="button"
																				 onClick={() => this.props.collapse_input("no", this.props.input.id)}>
																	Expand
																</button>
		
		if( this.props.input.is_collapse == "no" || this.props.input.is_collapse === false ){
			div_expand_or_collapse =  <button className="btn btn-link" type="button"
																				onClick={() => this.props.collapse_input("yes", this.props.input.id)}>
																	Collapse
																</button>
			
			div_formats = <div className={classname("btn-group",classes["left"], classes["fixButtonWidth"])} role="group" aria-label="Basic example">
											<button type="button"
															onClick={() => this.props.chooseOutputFormat(this.props.input.id, 'plain')}
															data-format='plain'
															className={this.showActiveClass('plain')}>Plain</button>
											<button type="button" onClick={this.handleFormatChange}
															onClick={() => this.props.chooseOutputFormat(this.props.input.id, 'md')}
															data-format='md'
															className={this.showActiveClass('md')}>Markdown</button>
											<button type="button"
															onClick={() => this.props.chooseOutputFormat(this.props.input.id, 'html')}
															data-format='html'
															className={this.showActiveClass('html')}>Html</button>

										</div>

			div_area = <textarea className='form-control' rows='5' id='input'
															 value={this.props.input.text}
															 onChange={this.handleChange}
															 onFocus={this.handleChange} />

			div_output = <div className={classname(classes['inputBox'],  classes['left'], classes['outputBox'])}>
											<label>Output#{this.props.index + 1}:</label>
											{tmp_output}
										</div>


		} 



		const save_button = <button className="btn btn-secondary" type="button"
														 onClick={() => this.props.update_item_title_and_hide_editable(this.props.input.id, this.input_title)}>
																				Save
											 </button>
		const delete_button = <button className="btn btn-link" type="button"
																	onClick={() => this.props.delete_input_local( this.props.input.id )}>
													  Delete
												  </button>

		//===========================================
		const div_title = <EditableTitle
												 update_item_title_and_hide_editable={this.props.update_item_title_and_hide_editable}
												 make_item_editable={this.props.set_item_title_editable}
												 index={this.props.index}
												 input_id={this.props.input.id}
												 update_title={this.props.input.update_title}
												 save_button={save_button}
												 delete_button={delete_button}
												 div_expand_or_collapse={div_expand_or_collapse}
												 my_title={this.props.input.title} />




		return(
				<div onDragStart={this.drapStartHandler.bind(this)}
						 onDrag={this.draggingHanlder.bind(this)}
						 draggable="true"
						 data-uid={this.props.input.id}
						 data-position={this.props.index}
						 onDragOver={this.dragoverHandler.bind(this)}
						 onDragLeave={this.dragLeaveHandler.bind(this)}
						 onDrop={this.onDropHandler.bind(this)}
						 className={classname('form-group', classes['inputBoxWrapper'])} key={this.props.input.id}>

					{div_title}

					{div_formats}
					{div_area}
					{div_output}
				</div>
		)
	}
}

LinkedInputBox.propTypes = {
	input: PropTypes.shape({
		id: PropTypes.string.isRequired,
		format: PropTypes.string.isRequire
	}).isRequired,
	index: PropTypes.number.isRequired,
	boxChangeEvent: PropTypes.func.isRequired,
	chooseOutputFormat: PropTypes.func.isRequired,
	saveInputPeriodically: PropTypes.func.isRequired,
	delete_input_local: PropTypes.func.isRequired,
	collapse_input: PropTypes.func.isRequired,
	input_size: PropTypes.number.isRequired,
	sort_inputs: PropTypes.func.isRequired,
	update_item_title_and_hide_editable: PropTypes.func.isRequired
}

export default LinkedInputBox

