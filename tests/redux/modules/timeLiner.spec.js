import {
		SAVE_SNAPSHOT, PAST_SNAPSHOT_BY_STEP, FUTURE_SNAPSHOT_BY_STEP,
		CHOOSE_NEXT_FUTURE, CHANGE_PAST_FROM_PRESENT, CHOOSE_CURRENT_SNAPSHOT,
		save_snapshot, past_snapshot_by_step, future_snapshot_by_step, choose_current_snapshot,
		request_prev_history_frame, request_next_history_frame, get_snapshot_with_hash,
		only_freeze_time_from_present_to_time_travel,
		choose_next_future, change_past_from_present,
		default as timeLineEnhancer
} from 'redux/modules/timeLiner'
import {random_hash} from '../../../src/concerns/helpers'
import undoable from 'redux-undo';

describe('(Redux Module) timePadReducer', function () {

	const store_state = 'this is my state'
	const expect_initial_state = {
		snapshots: [],
		history_frames: [],
		current_snapshot: '',
		time_status: {
			mode: 'present',
			viewed_snapshot: '',
			parent: '',
			children: [],
			snapshot_siblings: [],
			present_freeze: ''
		},
		past: [],
		present: store_state,
		future: []
	}

	//testing sinon.js mock object to replace undoable object
	// at http://sinonjs.org/docs/
	//const mock_undoable = sinon.mock(undoable)
	//const my_timeLineReducer = timeLineReducer(mock_undoable)
	const undoable_init_state = {
		past: [],
		present: store_state,
		future: []
	}
	const undoable = (state = undoable_init_state, action) => {
		return state
	}

	const timeLineReducer = timeLineEnhancer(undoable)


	describe('(Constants) timeLiner', function () {
		it('Should export the constants: PAST_SNAPSHOT_BY_STEP, FUTURE_SNAPSHOT_BY_STEP,' +
				'CHOOSE_NEXT_FUTURE, CHANGE_PAST_FROM_PRESENT.', function () {
			expect(PAST_SNAPSHOT_BY_STEP).to.equal('PAST_SNAPSHOT_BY_STEP')
			expect(FUTURE_SNAPSHOT_BY_STEP).to.equal('FUTURE_SNAPSHOT_BY_STEP')
			expect(CHOOSE_NEXT_FUTURE).to.equal('CHOOSE_NEXT_FUTURE')
			expect(CHANGE_PAST_FROM_PRESENT).to.equal('CHANGE_PAST_FROM_PRESENT')
		})
	})

	describe('(Reducer) timeLineReducer', function () {

		it('Should be a function.', function () {
			expect(timeLineReducer).to.be.a('function')
		})

		it('Should have proper initial properties.', function () {
			expect(timeLineReducer(undefined, {})).to.deep.equal(expect_initial_state)
		})

		it('Should trigger undoable function for first and second time', function () {
			const undoable_spy = sinon.spy(undoable)
			expect(undoable_spy.callCount).to.equal(0)
			const timeLineReducer = timeLineEnhancer(undoable_spy)
			expect(undoable_spy.callCount).to.equal(1)

			// In higher order function
			timeLineReducer(undefined, {})
			expect(undoable_spy.callCount).to.equal(2)

			// action trigger timeLineReducer action but not undoable function
			const timeLineAction = past_snapshot_by_step(1)
			timeLineReducer(expect_initial_state, timeLineAction)
			expect(undoable_spy.callCount).to.equal(2)
		})


	})

	describe('(Action/reducer) save_snapshot', function () {

		//const store_state = 'this is my state'
		const my_hash = random_hash()
		const my_descript = 'this is my description here'
		const expect_action = {
			type: SAVE_SNAPSHOT,
			payload: {
				description: my_descript,
				hash: my_hash
			}
		}

		const proper_action1 = save_snapshot(my_hash, my_descript)

		const initial_state1 = {
			snapshots: [
				{
					id: my_hash,
					state: store_state
				}
			],
			history_frames: [
				{
					id: my_hash,
					description: my_descript,
					children: [],
					parent: ''
				}
			],
			current_snapshot: my_hash,
			time_status: {
				mode: 'present',
				viewed_snapshot: my_hash,
				parent: '',
				children: [],
				snapshot_siblings: [my_hash],
				present_freeze: ''
			},
			past: [],
			present: store_state,
			future: []
		}

		it('Should return save_snapshot action.', function () {
			expect(proper_action1).to.deep.equal(expect_action)
		})

		it('Should save state using save_snapshot.', function () {
			const state = timeLineReducer(undefined, proper_action1)
			expect(state).to.deep.equal(initial_state1)
		})

		it('Should use save_snapshot the second time properly.', function () {
			const new_hash = random_hash()
			const new_descript = 'this is my new description 2'
			const new_snapshot_state = 'this a new state would be saved as snapshot'
			let time_line_state = timeLineReducer(undefined, proper_action1)
			time_line_state = Object.assign({}, time_line_state, {present: new_snapshot_state})
			const proper_action2 = save_snapshot(new_hash, new_descript)
			const expect_time_line_state2 = {
				snapshots: [
					{
						id: my_hash,
						state: store_state
					},
					{
						id: new_hash,
						state: new_snapshot_state
					}
				],
				history_frames: [
					{
						id: my_hash,
						description: my_descript,
						children: [new_hash],
						parent: ''
					},
					{
						id: new_hash,
						description: new_descript,
						children: [],
						parent: my_hash
					}
				],
				current_snapshot: new_hash,
				time_status: {
					mode: 'present',
					viewed_snapshot: new_hash,
					parent: my_hash,
					children: [],
					snapshot_siblings: [new_hash],
					present_freeze: ''
				},
				past: [],
				present: new_snapshot_state,
				future: []
			}

			const state = timeLineReducer(time_line_state, proper_action2)
			expect(state).to.deep.equal(expect_time_line_state2)
		})

		describe('(Action/reducer) (past_snapshot_by_step) and (future_snapshot_by_step)', function () {

			const my_step = 1
			const my_state = 'this my state for testing past_snapshot_by_step'
			const my_new_state_1 = 'cookie yummy banana orange'
			const expect_action1 = {
				type: PAST_SNAPSHOT_BY_STEP,
				payload: {
					step_size: my_step,
					present_freeze: my_new_state_1
				}
			}

			const proper_action1 = past_snapshot_by_step(my_step, my_new_state_1)
			const default_state = {
				snapshots: [],
				history_frames: [],
				current_snapshot: '',
				time_status: {
					mode: 'present',
					viewed_snapshot: '',
					parent: '',
					children: [],
					snapshot_siblings: [],
					present_freeze: ''
				}
			}

			const hash_arr = ['hash0', 'hash1', 'hash2', 'hash3', 'hash4',
				'hash5', 'hash6', 'hash7', 'hash8', 'hash9']
			const state_arr = ['my state1', 'my state2', 'my state3',
				'my state4', 'my state5', 'my state6', 'my state7', 'my hash8', 'my hash9']
			const my_snapshots = [
				{
					id: hash_arr[0],
					state: state_arr[0]
				},
				{
					id: hash_arr[1],
					state: state_arr[1]
				},
				{
					id: hash_arr[2],
					state: state_arr[2]
				},
				{
					id: hash_arr[3],
					state: state_arr[3]
				},
				{
					id: hash_arr[4],
					state: state_arr[4]
				},
				{
					id: hash_arr[5],
					state: state_arr[5]
				},
				{
					id: hash_arr[6],
					state: state_arr[6]
				},
				{
					id: hash_arr[7],
					state: state_arr[7]
				},
				{
					id: hash_arr[8],
					state: state_arr[8]
				},
				{
					id: hash_arr[9],
					state: state_arr[9]
				}
			]

			const my_history_frames = [
				{
					id: hash_arr[0],
					description: '',
					children: [hash_arr[1], hash_arr[4]],
					parent: ''
				},
				{
					id: hash_arr[1],
					description: '',
					children: [hash_arr[2], hash_arr[3]],
					parent: hash_arr[0]
				},
				{
					id: hash_arr[2],
					description: '',
					children: [hash_arr[8], hash_arr[9]],
					parent: hash_arr[1]
				},
				{
					id: hash_arr[3],
					description: '',
					children: [],
					parent: hash_arr[1]
				},
				{
					id: hash_arr[4],
					description: '',
					children: [hash_arr[5]],
					parent: hash_arr[0]
				},
				{
					id: hash_arr[5],
					description: '',
					children: [hash_arr[6]],
					parent: hash_arr[4]
				},
				{
					id: hash_arr[6],
					description: '',
					children: [hash_arr[7]],
					parent: hash_arr[5]
				},
				{
					id: hash_arr[7],
					description: '',
					children: [],
					parent: hash_arr[6]
				},
				{
					id: hash_arr[8],
					description: '',
					children: [],
					parent: hash_arr[2]
				},
				{
					id: hash_arr[9],
					description: '',
					children: [],
					parent: hash_arr[2]
				}
			]

			const time_line_state3 = {
				snapshots: my_snapshots,
				history_frames: my_history_frames,
				current_snapshot: hash_arr[3],
				time_status: {
					mode: 'present',
					viewed_snapshot: hash_arr[3],
					parent: hash_arr[1],
					children: [],
					snapshot_siblings: [],
					present_freeze: ''
				},
				past: [],
				present: state_arr[3],
				future: []
			}

			const expected_time_line_state1 = {
				snapshots: my_snapshots,
				history_frames: my_history_frames,
				current_snapshot: hash_arr[3],
				time_status: {
					mode: 'time_travel',
					viewed_snapshot: hash_arr[1],
					parent: hash_arr[0],
					children: [hash_arr[2], hash_arr[3]],
					//current frame wouldn't know about its sibling nodes. siblings would be reveal when going to the future
					snapshot_siblings: [],
					present_freeze: state_arr[3]
				},
				past: [ state_arr[3] ],
				present: state_arr[1],
				future: []
			}

			/*
			 hash0
			 - hash1
			 - hash2
			 - hash8
			 - hash9
			 - hash3
			 - hash4
			 - hash5
			 - hash6
			 - hash7
			 */

			describe('(Helper) request_prev_history_frame', function () {
				it('Should find the history_frame based on current viewed_snapshot and size', function () {
					//expect('1').to.equal('1')
					const actual_frame1 = request_prev_history_frame(time_line_state3, 1)
					const expected_frame1 = {
						id: hash_arr[1],
						description: '',
						children: [hash_arr[2], hash_arr[3]],
						parent: hash_arr[0]
					}
					const actual_frame2 = request_prev_history_frame(time_line_state3, 2)
					const expected_frame2 = {
						id: hash_arr[0],
						description: '',
						children: [hash_arr[1], hash_arr[4]],
						parent: ''
					}

					expect(actual_frame1).to.deep.equal(expected_frame1)
					expect(actual_frame2).to.deep.equal(expected_frame2)
				})
			})

			describe('(Helper) request_next_history_frame', function(){
				it('Should return no-child using request_next_history_framed when there is no next snapshot', function(){
					//expect('1').to.equal('1')
					const actual_frame1 = request_next_history_frame(time_line_state3, 1)
					expect(actual_frame1).to.deep.equal('no-child')
				})

				it('Should return next history frame using request_next_history_frame (assuming one child only)', function(){
					const time_line_state4 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'time_travel',
							viewed_snapshot: hash_arr[4],
							parent: hash_arr[0],
							children: [ hash_arr[5] ],
							snapshot_siblings: [ hash_arr[1], hash_arr[4] ],
							present_freeze: my_state
						}
					}

					const expected_history_frame4 = {
						id: hash_arr[5],
						description: '',
						children: [ hash_arr[6] ],
						parent: hash_arr[4]
					}

					const expected_history_frame5 = {
						id: hash_arr[7],
						description: '',
						children: [],
						parent: hash_arr[6]
					}

					const actual_frame1 = request_next_history_frame(time_line_state4, 1)
					expect(actual_frame1).to.deep.equal(expected_history_frame4)

					// when it is the last history_frame
					const actual_frame2 = request_next_history_frame(time_line_state4, 3)
					expect(actual_frame2).to.deep.equal(expected_history_frame5)

					// test scenario when no more next left
					const actual_frame3 = request_next_history_frame(time_line_state4, 10)
					expect(actual_frame3).to.deep.equal(expected_history_frame5)

				})

			})

			describe('(Action/reducer) (past_snapshot_by_step)', function(){

				const past_snapshot_action = past_snapshot_by_step(1)

				it('Should return past_snapshot_by_step action.', function () {
					const expected_action = {
						type: PAST_SNAPSHOT_BY_STEP,
						payload: {
							step_size: 1
						}
					}
					expect(past_snapshot_action).to.deep.equal(expected_action)
				})

				it('Should show default state using past_snapshot_by_step.', function () {
					const state = timeLineReducer(undefined, past_snapshot_action)
					expect(state).to.deep.equal(expect_initial_state)
				})

				it('Should return previous state if the present is not ' +
					'changed and update past and present attribute.', function () {
					const state = timeLineReducer(time_line_state3, past_snapshot_action)
					expect(state).to.deep.equal(expected_time_line_state1)
				})

				it('Should return latest state if the present is different from' +
						' latest snapshot and it is viewed_snapshot is latest snapshot', function () {
					const new_present = 'apple banana'
					const time_line_state4 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'present',
							viewed_snapshot: hash_arr[3],
							parent: hash_arr[1],
							children: [],
							snapshot_siblings: [],
							present_freeze: ''
						},
						past: [],
						present: new_present,
						future: []
					}

					const expected_time_line_state4 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'time_travel',
							viewed_snapshot: hash_arr[3],
							parent: hash_arr[1],
							children: [],
							snapshot_siblings: [],
							present_freeze: new_present
						},
						past: [ new_present ],
						present: state_arr[3],
						future: []
					}

					const state = timeLineReducer(time_line_state4, past_snapshot_action)
					expect(state).to.deep.equal(expected_time_line_state4)


					//Users cannot jump more than 2 steps if current_snapshot is also latest snapshot and
					//the present is different from latest_snapshot state
					const proper_action2 = past_snapshot_by_step(8)
					const state2 = timeLineReducer(time_line_state4, proper_action2)
					expect(state2).to.deep.equal(expected_time_line_state4)
				})

				it('Should return proper state using past_snapshot_by_step for 2 steps back.' +
						'user can only move two steps back when the present has not been modified ', function () {
					const proper_action2 = past_snapshot_by_step(2)
					const new_present = 'apple banana 2'
					const state = timeLineReducer(time_line_state3, proper_action2)
					const expected_time_line_state2 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'time_travel',
							viewed_snapshot: hash_arr[0],
							parent: '',
							children: [ hash_arr[1], hash_arr[4] ],
							snapshot_siblings: [],
							present_freeze: state_arr[3]
						},
						past: [ state_arr[3] ],
						present: state_arr[0],
						future: []
					}

					expect(state).to.deep.equal(expected_time_line_state2)
				})

				it("Should only freeze present one when its transition from present to time-travel mode", function(){
					const proper_action1 = past_snapshot_by_step(1)
					const new_present = 'apple banana 2'
					const time_line_state3 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'present',
							viewed_snapshot: hash_arr[8],
							parent: hash_arr[2],
							children: [],
							snapshot_siblings: [],
							present_freeze: ''
						},
						past: [],
						present: new_present,
						future: []
					}
					const state_1 = timeLineReducer(time_line_state3, proper_action1)
					const expected_time_line_state1 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'time_travel',
							viewed_snapshot: hash_arr[2],
							parent: hash_arr[1],
							children: [ hash_arr[8], hash_arr[9] ],
							snapshot_siblings: [],
							present_freeze: new_present
						},
						past: [ new_present ],
						present: state_arr[2],
						future: []
					}
					const state_2 = timeLineReducer(state_1, proper_action1)
					const expected_time_line_state2 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'time_travel',
							viewed_snapshot: hash_arr[1],
							parent: hash_arr[0],
							children: [ hash_arr[2], hash_arr[3] ],
							snapshot_siblings: [],
							present_freeze: new_present
						},
						past: [ new_present, state_arr[2] ],
						present: state_arr[1],
						future: []
					}

					expect(state_1).to.deep.equal(expected_time_line_state1)
					expect(state_2).to.deep.equal(expected_time_line_state2)
				})
			})

			describe('(Action/reducer) (future_snapshot_by_step)', function() {

				const expected_action2 = {
					type: FUTURE_SNAPSHOT_BY_STEP,
					payload: {
						step_size: my_step
					}
				}

				const my_future_present = 'this is my future present state blablabla'
				const proper_action2 = future_snapshot_by_step(my_step)

				const initial_time_line_state2 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[4],
						parent: hash_arr[0],
						children: [hash_arr[5]],
						snapshot_siblings: [],
						present_freeze: my_state
					},
					past: [],
					present: my_future_present,
					future: []
				}

				const expected_time_line_state2 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[5],
						parent: hash_arr[4],
						children: [hash_arr[6]],
						snapshot_siblings: [hash_arr[5]],
						present_freeze: my_state
					},
					past: [my_future_present],
					present: state_arr[5],
					future: []
				}

				const expected_time_line_state3 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[6],
						parent: hash_arr[5],
						children: [hash_arr[7]],
						snapshot_siblings: [hash_arr[6]],
						present_freeze: my_state
					},
					past: [ my_future_present ],
					present: state_arr[6],
					future: []
				}

				const expected_time_line_state4 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[7],
						parent: hash_arr[6],
						children: [],
						snapshot_siblings: [hash_arr[7]],
						present_freeze: my_state
					},
					past: [ my_future_present ],
					present: state_arr[7],
					future: []
				}

				it('Should return future_snapshot_by_step action.', function () {
					expect(proper_action2).to.deep.equal(expected_action2)
				})

				it('Should show default state using future_snapshot_by_step', function () {
					const state = timeLineReducer(undefined, proper_action2)
					expect(state).to.deep.equal(expect_initial_state)
				})

				it('Should return proper state using future_snapshot_by_step ' +
					'for one step and present_freeze stays the same.', function () {
					const state = timeLineReducer(initial_time_line_state2, proper_action2)
					expect(state).to.deep.equal(expected_time_line_state2)
				})

				it('Should return proper state using future_snapshot_by_step for two steps.', function () {
					const proper_action3 = future_snapshot_by_step(2)
					const state = timeLineReducer(initial_time_line_state2, proper_action3)
					expect(state).to.deep.equal(expected_time_line_state3)
				})

				it('Should return proper state using future_snapshot_by_step for three steps.', function () {
					const proper_action4 = future_snapshot_by_step(3)
					const state = timeLineReducer(initial_time_line_state2, proper_action4)
					expect(state).to.deep.equal(expected_time_line_state4)
				})

				it('Should return proper state using future_snapshot_by_step for last child and beyond.', function () {
					const proper_action5 = future_snapshot_by_step(10, my_state)
					const state = timeLineReducer(initial_time_line_state2, proper_action5)
					expect(state).to.deep.equal(expected_time_line_state4)
				})

				it('Should return proper state using future_snapshot_by_step ' +
					'and ensure state freeze is only done when it ' +
					'transitions from "present" to "time_travel" mode.',
					function () {
						const new_state = 'yo yo candy pie'
						const initial_time_line_state2 = {
							snapshots: my_snapshots,
							history_frames: my_history_frames,
							current_snapshot: hash_arr[3],
							time_status: {
								mode: 'present',
								viewed_snapshot: hash_arr[4],
								parent: hash_arr[0],
								children: [hash_arr[5]],
								snapshot_siblings: [],
								present_freeze: my_future_present
							},
							past: [my_future_present],
							present: new_state,
							future: []
						}

						const expected_time_line_state3 = {
							snapshots: my_snapshots,
							history_frames: my_history_frames,
							current_snapshot: hash_arr[3],
							time_status: {
								mode: 'time_travel',
								viewed_snapshot: hash_arr[6],
								parent: hash_arr[5],
								children: [ hash_arr[7] ],
								snapshot_siblings: [ hash_arr[6] ],
								present_freeze: new_state
							},
							past: [ my_future_present, new_state ],
							present: state_arr[6],
							future: []
						}

						const expected_time_line_state4 = {
							snapshots: my_snapshots,
							history_frames: my_history_frames,
							current_snapshot: hash_arr[3],
							time_status: {
								mode: 'time_travel',
								viewed_snapshot: hash_arr[7],
								parent: hash_arr[6],
								children: [],
								snapshot_siblings: [ hash_arr[7] ],
								present_freeze: new_state
							},
							past: [ my_future_present, new_state, state_arr[6] ],
							present: state_arr[7],
							future: []
						}
						const proper_action3 = future_snapshot_by_step(2)
						const state3 = timeLineReducer(initial_time_line_state2, proper_action3)
						const state4 = timeLineReducer(state3, proper_action3)
						expect(state3).to.deep.equal(expected_time_line_state3)
						expect(state4).to.deep.equal(expected_time_line_state4)
					})
			})

			describe('(Action/reducer) (choose_next_future)', function () {

				const my_hash = hash_arr[2]
				const my_state3 = 'apple yummy pie'
				const expected_action3 = {
					type: CHOOSE_NEXT_FUTURE,
					payload: {
						chosen_future_hash: my_hash
					}
				}

				const proper_action3 = choose_next_future(my_hash)

				const initial_time_line_state3 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[1],
						parent: hash_arr[0],
						children: [hash_arr[2], hash_arr[3]],
						snapshot_siblings: [],
						present_freeze: my_state
					},
					past: [],
					present: my_state3,
					future: []
				}

				const expected_time_line_state3 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[2],
						parent: hash_arr[1],
						children: [hash_arr[8], hash_arr[9]],
						snapshot_siblings: [hash_arr[2], hash_arr[3]],
						present_freeze: my_state
					},
					past: [my_state3],
					present: state_arr[2],
					future: []
				}

				it('Should return choose_next_future action.', function () {
					expect(proper_action3).to.deep.equal(expected_action3)
				})

				it('Should show default state using choose_next_future', function () {
					const state = timeLineReducer(undefined, expected_action3)
					expect(state).to.deep.equal(expect_initial_state)
				})

				it('Should return proper state using choose_next_future and proper children.', function () {
					const state = timeLineReducer(initial_time_line_state3, expected_action3)
					expect(state).to.deep.equal(expected_time_line_state3)
				})

				it('Should return proper state using choose_next_future (test2) and update present_freeze.', function () {
					const my_present3 = 'yummy noodle'
					const initial_time_line_state4 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'present',
							viewed_snapshot: hash_arr[0],
							parent: '',
							children: [ hash_arr[1], hash_arr[4] ],
							snapshot_siblings: [],
							present_freeze: my_state
						},
						past: [],
						present: my_present3,
						future: []
					}

					const expected_time_line_state4 = {
						snapshots: my_snapshots,
						history_frames: my_history_frames,
						current_snapshot: hash_arr[3],
						time_status: {
							mode: 'time_travel',
							viewed_snapshot: hash_arr[4],
							parent: hash_arr[0],
							children: [ hash_arr[5] ],
							snapshot_siblings: [ hash_arr[1], hash_arr[4] ],
							present_freeze: my_present3
						},
						past: [my_present3],
						present: state_arr[4],
						future: []
					}

					const proper_action4 = choose_next_future(hash_arr[4], my_state3)

					const state = timeLineReducer(initial_time_line_state4, proper_action4)
					expect(state).to.deep.equal(expected_time_line_state4)
				})
			})

			describe('(Action/reducer) (change_past_from_present)', function () {
				const my_hash4 = hash_arr[4]
				const expected_action4 = {
					type: CHANGE_PAST_FROM_PRESENT,
					payload: {
						my_chosen_past: my_hash4
					}
				}
				const my_present4 = 'random present time stuff'
				const proper_action4 = change_past_from_present(my_hash4)
				const initial_time_line_state4 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[1],
						parent: hash_arr[0],
						children: [hash_arr[2], hash_arr[3]],
						snapshot_siblings: [],
						present_freeze: my_state
					},
					past: [],
					present: my_present4,
					future: []
				}

				const expected_time_line_state4 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: my_hash4,
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[1],
						parent: hash_arr[0],
						children: [hash_arr[2], hash_arr[3]],
						snapshot_siblings: [],
						present_freeze: my_state
					},
					past: [],
					present: my_present4,
					future: []
				}

				it('Should return change_past_from_present action.', function () {
					expect(proper_action4).to.deep.equal(expected_action4)
				})

				it('Should show default state using change_past_from_present', function () {
					const state = timeLineReducer(undefined, expected_action4)
					expect(state).to.deep.equal(expect_initial_state)
				})

				it('Should return proper state using change_past_from_present .', function () {
					const state = timeLineReducer(initial_time_line_state4, expected_action4)
					expect(state).to.deep.equal(expected_time_line_state4)
				})

			})

			describe('(Helper) (get_snapshot_with_hash)', function () {
				const my_hash4 = hash_arr[4]
				const expected_snapshot = {
					id: hash_arr[4],
					state: state_arr[4]
				}

				it('Should get the snapshot based on snapshot hash.', function () {
					const actual_1 = get_snapshot_with_hash(my_snapshots, my_hash4)
					expect(actual_1).to.deep.equal(expected_snapshot)
				})
			})

			describe('(Helper) (only_freeze_time_from_present_to_time_travel)', function () {
				const my_present4 = 'I am the present now'
				const initial_time_line_state4 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[1],
						parent: hash_arr[0],
						children: [hash_arr[2], hash_arr[3]],
						snapshot_siblings: [],
						present_freeze: my_state
					},
					past: [],
					present: my_present4,
					future: []
				}

				const initial_time_line_state5 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'present',
						viewed_snapshot: hash_arr[1],
						parent: hash_arr[0],
						children: [hash_arr[2], hash_arr[3]],
						snapshot_siblings: [],
						present_freeze: my_state
					},
					past: [],
					present: my_present4,
					future: []
				}

				it('Should return "present_freeze" if mode is "time_travel" .', function () {
					const actual_1 = only_freeze_time_from_present_to_time_travel(initial_time_line_state4)
					expect(actual_1).to.deep.equal(my_state)
				})

				it('Should return "present" if mode is "present" .', function () {
					const actual_1 = only_freeze_time_from_present_to_time_travel(initial_time_line_state5)
					expect(actual_1).to.deep.equal(my_present4)
				})
			})


			describe('(Action/reducer) (choose_current_snapshot)', function () {
				const my_hash = hash_arr[6]
				const expected_action = {
					type: CHOOSE_CURRENT_SNAPSHOT,
					payload: {
						chosen_hash: my_hash
					}
				}
				const proper_action = choose_current_snapshot(my_hash)
				const time_line_state = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'present',
						viewed_snapshot: hash_arr[3],
						parent: hash_arr[1],
						children: [],
						snapshot_siblings: [],
						present_freeze: ''
					},
					past: [],
					present: state_arr[3],
					future: []
				}

				const expected_time_line_state1 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[6],
						parent: hash_arr[5],
						children: [ hash_arr[7] ],
						snapshot_siblings: [],
						present_freeze: state_arr[3]
					},
					past: [ state_arr[3] ],
					present: state_arr[6],
					future: []
				}


				it('Should return choose_current_snapshot action. ', function () {
					expect(proper_action).to.deep.equal(expected_action)
				})

				it('Should show default state using choose_current_snapshot', function () {
					const state = timeLineReducer(undefined, proper_action)
					expect(state).to.deep.equal(expect_initial_state)
				})

				it('Should move to the selected snapshot using choose_current_snapshot', function () {
					const state = timeLineReducer(time_line_state, proper_action)
					expect(state).to.deep.equal(expected_time_line_state1)
				})

			})

			describe('(Action/reducer) (switch_mode)', function () {
				const my_hash = hash_arr[6]
				const expected_action = {
					type: CHOOSE_CURRENT_SNAPSHOT,
					payload: {
						chosen_hash: my_hash
					}
				}
				const proper_action = choose_current_snapshot(my_hash)
				const time_line_state = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					mode: '',
					time_status: {
						mode: 'present',
						viewed_snapshot: hash_arr[3],
						parent: hash_arr[1],
						children: [],
						snapshot_siblings: [],
						present_freeze: ''
					},
					past: [],
					present: state_arr[3],
					future: []
				}

				const expected_time_line_state1 = {
					snapshots: my_snapshots,
					history_frames: my_history_frames,
					current_snapshot: hash_arr[3],
					time_status: {
						mode: 'time_travel',
						viewed_snapshot: hash_arr[6],
						parent: hash_arr[5],
						children: [ hash_arr[7] ],
						snapshot_siblings: [],
						present_freeze: state_arr[3]
					},
					past: [ state_arr[3] ],
					present: state_arr[6],
					future: []
				}
				it('Should return switch_mode action. ', function () {
					console.log("Can you see me here at 1071")
					//expect(proper_action).to.deep.equal(expected_action)
				})
			})




		})
	})
})
