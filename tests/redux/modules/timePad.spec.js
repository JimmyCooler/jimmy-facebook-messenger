import {
	NOTE_INCREMENT, SAVE_INPUT_BOX, CHOOSE_OUTPUT_FORM, SAVE_INPUT_PERIODICALLY,
	increment, saveInputBox, chooseOutputFormat, saveInputPeriodically,
	default as timePadReducer
} from 'redux/modules/timePad'

describe('(Redux Module) timePadReducer', function () {


	// -----------------------------------
	// Test constants
	// -----------------------------------
	describe('(Redux Module) timePad Constants', function() {
		it('Should export a constant NOTE_INCREMENT.', function () {
			expect(NOTE_INCREMENT).to.equal('NOTE_INCREMENT')
		})

		it('Should export a constant SAVE_INPUT_BOX.', function () {
			expect(SAVE_INPUT_BOX).to.equal('SAVE_INPUT_BOX')
		})

		it('Should export a constant CHOOSE_OUTPUT_FORM.', function () {
			expect(CHOOSE_OUTPUT_FORM).to.equal('CHOOSE_OUTPUT_FORM')
		})

		it('Should export a constant COUNTER_INCREMENT.', function () {
			expect(SAVE_INPUT_PERIODICALLY).to.equal('SAVE_INPUT_PERIODICALLY')
		})
	})


	describe('(Reducer) timePadReducer', function () {
		it('Should be a function.', function () {
			expect(timePadReducer).to.be.a('function')
		})

		it('Should initialize with a state of inputs:[].', function () {
			expect(timePadReducer(undefined, {})).to.deep.equal({inputs: []})
		})

		it('Should return the previous state if an action was not matched.', function () {
			let state = timePadReducer(undefined, {})
			expect(state).to.deep.equal({inputs: []})

			const old_state = timePadReducer(state, {type: '@@@@@@@'})
			expect(state).to.deep.equal({inputs: []})

			const random_index = 'index-1'
			const new_state = timePadReducer(old_state, increment(random_index))
			const expect_input = {
				id: random_index,
				text: '',
				output: '',
				format: 'plain'
			}
			expect(new_state).to.deep.equal({inputs: [expect_input]})


			const new_state_1 = timePadReducer(new_state, {type: '@@@@@@@'})
			expect(new_state_1).to.deep.equal(new_state)
		})
	})

	describe('(Action/reducer) increment', function () {
		  const random_index = 'index-1'
		  const random_index2 = 'index-2'
			const expect_action = {
															type: NOTE_INCREMENT,
															payload: random_index
														}
			it('Should return note_increment action.', function () {
				expect(increment(random_index)).to.deep.equal(expect_action)
			})

			it('should add multiple inputboxes', function() {
				const state = timePadReducer(state, increment(random_index))
				const expect_input = {
					id: random_index,
					text: '',
					output: '',
					format: 'plain'
				}

				expect(state).to.deep.equal({inputs:[expect_input]})

				const expect_input2 = {
					id: random_index2,
					text: '',
					output: '',
					format: 'plain'
				}

				const state2 = timePadReducer(state, increment(random_index2))
				expect(state2).to.deep.equal({inputs:[expect_input, expect_input2]})
			})
		})

	describe('(Action/reducer) saveInputBox', function () {
			const index = 'hello-index';
			const input_text = 'hello world'
			const expect_action = {
				type: SAVE_INPUT_BOX,
				payload: {
					id: index,
					text: input_text,
					output: input_text
				}
			}
			it('Should return saveInputBox action.', function () {
				expect(saveInputBox(index, input_text)).to.deep.equal(expect_action)
			})

			it('Should return new output and text if format = md.', function () {
				const state = timePadReducer(undefined, increment(index))
				const text_input = 'yummy yummy'
				const expect_input = {
					id: index,
					text: text_input,
					output: text_input,
					format: 'plain'
				}
				expect(timePadReducer(state, saveInputBox(index, text_input)))
					.to.deep.equal({inputs:[expect_input]})
			})

		})

	describe('(Action) chooseOutputFormat', function () {
			const index = 'index_for_chooseOutputFormat';
			const output_format = 'md'
			const expect_action = {
				type: CHOOSE_OUTPUT_FORM,
				payload: {
					id: index,
					format: output_format
				}
			}

			it('Should return chooseOutputFormat action.', function () {
				expect(chooseOutputFormat(index, output_format)).to.deep.equal(expect_action)
			})

		it('Should return chooseOutputFormat format state.', function () {
			const state = timePadReducer(undefined, increment(index))
			const expect_input = {
				id: index,
				text: '',
				output: '',
				format: 'md'
			}
			expect(timePadReducer(state, chooseOutputFormat(index, 'md')))
				.to.deep.equal({inputs:[expect_input]})
		})

	})


	describe('(Action) saveInputPeriodically', function () {
			const index = 'index_saveInputPeriodically';
			const input_text = 'hello world'
			const expect_action = {
				type: SAVE_INPUT_PERIODICALLY,
				payload: {
					id: index,
					text: input_text,
					output: input_text
				}
			}
			it('Should return note_increment saveInputPeriodically.', function () {
				expect(saveInputPeriodically(index, input_text)).to.deep.equal(expect_action)

			})
		})

	describe('(Action) update_inputs', function () {
		/*const index = 'index_saveInputPeriodically';
		const input_text = 'hello world'
		const expect_action = {
			type: SAVE_INPUT_PERIODICALLY,
			payload: {
				id: index,
				text: input_text,
				output: input_text
			}
		}
		it('Should return note_increment saveInputPeriodically.', function () {
			expect(saveInputPeriodically(index, input_text)).to.deep.equal(expect_action)

		})*/
	})

})
