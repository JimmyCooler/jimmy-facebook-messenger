import React from 'react'
import TestUtils from 'react-addons-test-utils'
import { bindActionCreators } from 'redux'
import { TimePadView } from 'views/TimePadView/TimePadView'
import { mount } from 'enzyme'
import App from 'components/TimePad'

function shallowRender (component) {
	const renderer = TestUtils.createRenderer()

	renderer.render(component)
	return renderer.getRenderOutput()
}

function renderWithProps (props = {}) {
	return TestUtils.renderIntoDocument(<TimePadView {...props} />)
}

function shallowRenderWithProps (props = {}) {
	return shallowRender(<TimePadView {...props} />)
}

describe('(View) TimePad', function () {
	let _component, _rendered, _props, _spies, unique_index

	/*beforeEach(function () {

		unique_index = 'blablab-here'
		_spies = {
			increment: sinon.spy()
		}
		_props = {
			inputs: [{
				id: unique_index,
				text: '',
				output: '',
				format: 'plain'
			}],
			history_frames: [
				{
					id: unique_index,
					description: '',
					children: [],
					parent: ''
				}
			],
			current_snapshot: '',
			timeLiner: {
				snapshots: [],
				history_frames: [],
				current_snapshot: '',
				time_status: {
					mode: 'present',
					viewed_snapshot: '',
					parent: '',
					children: [],
					snapshot_siblings: [],
					present_freeze: ''  //no need to freeze present since it is already in "present" mode
				}
			},
			...bindActionCreators({
				increment: _spies.increment,
			  update_inputs: (_spies.update_inputs = sinon.spy()),
				boxChangeEvent: (_spies.boxChangeEvent = sinon.spy()),
				chooseOutputFormat: (_spies.chooseOutputFormat = sinon.spy()),
				saveInputPeriodically: (_spies.saveInputPeriodically = sinon.spy()),
				undo: (_spies.undo = sinon.spy()),
				redo: (_spies.redo = sinon.spy()),
				save_snapshot: (_spies.save_snapshot = sinon.spy()),
				past_snapshot_by_step: (_spies.past_snapshot_by_step = sinon.spy()),
				future_snapshot_by_step: (_spies.future_snapshot_by_step = sinon.spy()),
				choose_next_future: (_spies.choose_next_future = sinon.spy()),
				change_past_from_present: (_spies.change_past_from_present = sinon.spy())
			}, _spies.dispatch = sinon.spy())
		}

		_component = shallowRenderWithProps(_props)
		_rendered = renderWithProps(_props)
	})

	it('Should render as a <App>.', function () {
		expect(_component.type).to.equal(App)
	})

	it('Should include an <h1> with title text.', function () {
		const textarea = TestUtils.findRenderedDOMComponentWithTag(_rendered, 'textarea')

		expect(textarea).to.exist
	})

	it('Should include one <textarea> element.', function () {
		const h1 = TestUtils.findRenderedDOMComponentWithTag(_rendered, 'h1')

		expect(h1).to.exist
		expect(h1.textContent).to.match(/Welcome to Time Lord's TimePad/)
	})

	it('Should contains Input#1 and Output#1.', function () {
		const wrapper = mount(<TimePadView {..._props} />)
		expect(wrapper.text()).to.contain("Input#1");
		expect(wrapper.text()).to.contain("Output#1");
	})*/



	/*describe('Describe all the buttons features', function(){
		let buttons, add_more_b, undo_b, redo_b, plain_b, md_b, html_b, diversion_b
		let save_b, prev_b, next_b
		beforeEach(function(){
			buttons = TestUtils.scryRenderedDOMComponentsWithTag(_rendered, 'button')
			add_more_b = buttons[0]
			undo_b = buttons[1]
			redo_b = buttons[2]

			save_b = buttons[3]
			prev_b = buttons[4]
			next_b = buttons[5]

			plain_b = buttons[6]
			md_b = buttons[7]
			html_b = buttons[8]
			diversion_b = buttons[9]

		})

		it('Should contain three main buttons on top and four buttons at input form', function () {

			expect(buttons).to.exist
			expect(buttons).to.have.length.of(10)

			expect(save_b.textContent).to.match(/Save/)
			expect(prev_b.textContent).to.match(/Prev/)
			expect(next_b.textContent).to.match(/Next/)

			expect(add_more_b.textContent).to.match(/Add More/)
			expect(undo_b.textContent).to.match(/Undo/)
			expect(redo_b.textContent).to.match(/Redo/)
			expect(plain_b.textContent).to.match(/Plain/)
			expect(md_b.textContent).to.match(/Markdown/)
			expect(html_b.textContent).to.match(/Html/)
			expect(diversion_b.textContent).to.match(/Diversion/)
		})

		it('has Add-more button click action', function () {
			// Please refer to src/views/TimePadView/TimePadView.js:13
			// for reason why it is triggerd twice initially.
			expect(_spies.increment.callCount).to.equal(2)
			TestUtils.Simulate.click(add_more_b)
			expect(_spies.increment.callCount).to.equal(3)
		})

		it('tests Undo button  ...', function () {
			expect(_spies.undo.callCount).to.equal(0)
			TestUtils.Simulate.click(undo_b)
			expect(_spies.undo.callCount).to.equal(1)
		})

		it('Has Save/Prev/Next button  ...', function () {
			expect(_spies.save_snapshot.callCount).to.equal(0)
			TestUtils.Simulate.click(save_b)
			expect(_spies.save_snapshot.callCount).to.equal(1)

			expect(_spies.past_snapshot_by_step.callCount).to.equal(0)
			TestUtils.Simulate.click(prev_b)
			expect(_spies.past_snapshot_by_step.callCount).to.equal(1)

			expect(_spies.future_snapshot_by_step.callCount).to.equal(0)
			TestUtils.Simulate.click(next_b)
			expect(_spies.future_snapshot_by_step.callCount).to.equal(1)
		})



		it('tests Redo button ...', function () {
			expect(_spies.redo.callCount).to.equal(0)
			TestUtils.Simulate.click(redo_b)
			expect(_spies.redo.callCount).to.equal(1)
		})

		it('tests Plain button ...', function () {
			expect(_spies.chooseOutputFormat.callCount).to.equal(0)
			TestUtils.Simulate.click(plain_b)
			expect(_spies.chooseOutputFormat.callCount).to.equal(1)
		})

		it('tests Markdown button ...', function () {
			expect(_spies.chooseOutputFormat.callCount).to.equal(0)
			TestUtils.Simulate.click(md_b)
			expect(_spies.chooseOutputFormat.callCount).to.equal(1)
		})

		it('tests Html button ...', function () {
			expect(_spies.chooseOutputFormat.callCount).to.equal(0)
			TestUtils.Simulate.click(html_b)
			expect(_spies.chooseOutputFormat.callCount).to.equal(1)
		})

		it('tests Diversion button ...', function () {
			expect(_spies.chooseOutputFormat.callCount).to.equal(0)
			TestUtils.Simulate.click(diversion_b)
			expect(_spies.chooseOutputFormat.callCount).to.equal(0)
		})

		it('test textarea input ...', function(){
			const textarea = TestUtils.findRenderedDOMComponentWithTag(_rendered, 'textarea')
			textarea.value = 'Jimmy is here'
			expect(_spies.boxChangeEvent.callCount).to.equal(0)
			TestUtils.Simulate.change(textarea)
			expect(_spies.boxChangeEvent.callCount).to.equal(1)

			let i = 0
			expect(_spies.saveInputPeriodically.callCount).to.equal(0)
			while(i<12){
				textarea.value = 'Jimmy is here ' + i
				TestUtils.Simulate.change(textarea)
				i++
			}

			expect(_spies.saveInputPeriodically.callCount).to.equal(1)
		})
	})
*/
})
