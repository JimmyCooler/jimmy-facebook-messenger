import {random_hash, get_tree_structure,
		get_current_timeline, get_timeline_nodes_with_current} from '../../src/concerns/helpers'

describe('(Helpers) functions', function () {

	const hash_arr = ['hash0', 'hash1', 'hash2', 'hash3', 'hash4',
		'hash5', 'hash6', 'hash7', 'hash8', 'hash9']

	const my_history_frames = [
		{
			id: hash_arr[0],
			description: '',
			children: [hash_arr[1], hash_arr[4]],
			parent: ''
		},
		{
			id: hash_arr[1],
			description: '',
			children: [hash_arr[2], hash_arr[3]],
			parent: hash_arr[0]
		},
		{
			id: hash_arr[2],
			description: '',
			children: [hash_arr[8], hash_arr[9]],
			parent: hash_arr[1]
		},
		{
			id: hash_arr[3],
			description: '',
			children: [],
			parent: hash_arr[1]
		},
		{
			id: hash_arr[4],
			description: '',
			children: [hash_arr[5]],
			parent: hash_arr[0]
		},
		{
			id: hash_arr[5],
			description: '',
			children: [hash_arr[6]],
			parent: hash_arr[4]
		},
		{
			id: hash_arr[6],
			description: '',
			children: [hash_arr[7]],
			parent: hash_arr[5]
		},
		{
			id: hash_arr[7],
			description: '',
			children: [],
			parent: hash_arr[6]
		},
		{
			id: hash_arr[8],
			description: '',
			children: [],
			parent: hash_arr[2]
		},
		{
			id: hash_arr[9],
			description: '',
			children: [],
			parent: hash_arr[2]
		}
	]

	/*
	 hash0
	 		-hash1
	 				-hash2
	 						-hash8
	 						-hash9
	 				-hash3
	 		-hash4
	 				-hash5
	 					-hash6
	 							-hash7
	 ==============================
	 hash7
	 hash8, hash9, hash6
	 hash2, hash3, hash5
	 hash1, hash4
	 hash0
	 */

	describe('helper => random_hash', function(){
		it('Should generate random hash value', function () {
			const actual_1 = random_hash()
			const actual_2 = random_hash()
			expect(actual_1).to.not.equal(actual_2)
			expect(actual_1.length).to.equal(10)
			expect(actual_1.length).to.equal(10)

			const actual_3 = random_hash(13)
			expect(actual_3).to.not.equal(actual_2)
			expect(actual_3.length).to.equal(13)
		})
	})

	describe('helper => generate_timeline_nodes', function(){
		it('should generate timeline_nodes based on the history_frame and root id ', function () {
			const actual = get_tree_structure(my_history_frames, ['hash0'], [])
			const expected =  [
				['hash7'], ['hash8', 'hash9', 'hash6'],
				['hash2', 'hash3', 'hash5'],
				['hash1', 'hash4'], ['hash0']]

			expect(actual).to.deep.equal(expected)
		})
	})

	describe('helper => get_current_timeline', function(){
		it('Should return an array representing the time line using current_hash_id and history_frames', function (){
			const actual = get_current_timeline(my_history_frames, 'hash3')
			const expected = [ 'hash3', 'hash1', 'hash0' ]
			expect(actual).to.deep.equal(expected)

			const actual2 = get_current_timeline(my_history_frames, 'hash7')
			const expected2 = ['hash7', 'hash6', 'hash5', 'hash4', 'hash0']
			expect(actual2).to.deep.equal(expected2)
		})
	})

	describe('helper => get_timeline_nodes_with_current', function(){
		it('should generate timeline_nodes based on the history_frame and current_id', function () {
			const actual = get_timeline_nodes_with_current(my_history_frames, 'hash3')
			const expected =  {
				tree:	[
					[{id:'hash7'}],
					[{id:'hash8'}, {id:'hash9'}, {id:'hash6'}],
					[{id:'hash2', is_current:'y'}, {id:'hash3', selected: 'y'}, {id:'hash5'}],
					[{id:'hash1', selected: 'y'}, {id:'hash4'}],
					[{id:'hash0', selected: 'y'}]
				],
				total: 10
			};

			/*
			 [
			 ['hash7'],
			 ['hash8', 'hash9', 'hash6'],
			 ['hash2', 'hash3', 'hash5'],
			 ['hash1', 'hash4'], ['hash0']
			 ]
			 */

			expect(actual).to.deep.equal(expected)
		})
	})


	/*describe('helper => memoization_object', function(){
		it('should add nodes that is not already exists', function () {
			let my_nodes = [
				{

				}
			]
			console.log("can you see me at 160");
		})
	})*/

})